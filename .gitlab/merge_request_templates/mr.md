Please ensure your pull request adheres to the following guidelines:

- [ ] Your code is covered by unit tests
- [ ] Delivered UI is responsive
- [ ] No errors / warnings appear in console or during tests
- [ ] All console.log and debug code are removed 

Thanks and good code !

import { initResources } from 'jeto';
import initDatabase from './src/server/init/database';
import initModels from './src/server/init/models';
import initConfig from './src/server/init/config';
import { cryptPassword } from './src/server/utils';
import { ROLE } from './src/server/models/roles';
import { STATUS } from './src/server/models/statuses';

import { task, logger, series } from 'just-task';

let CTX;
const resources = [initConfig, initDatabase, initModels];

task('setup', async () => {
  return initResources(resources).then(ctx => (CTX = ctx));
});

task('teardown', async () => {
  return CTX().mongo.close();
});

task('create-admin', async () => {
  const { models } = CTX();
  const password = '1234';
  const user = {
    firstname: 'eric',
    lastname: 'basley',
    password: cryptPassword(password),
    email: 'ebasley@gmail.com',
    roles: [ROLE.admin],
    status: STATUS.active,
  };

  await models.people.insertOne(user);
  logger.info(`admin user created ${user.email}#${password}`);
});

task('create-admin', series('setup', 'create-admin', 'teardown'));

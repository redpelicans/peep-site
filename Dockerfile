FROM node:14.1-alpine

RUN mkdir -p /opt
WORKDIR /opt

COPY build /opt/build
COPY node_modules /opt/node_modules
COPY dist /opt/dist
COPY .eslint* package.json yarn.lock just-task /opt/
COPY .env.production /opt/.env


EXPOSE 80
CMD yarn dist:run


import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    vacation: '#7986cb',
    dayOff: '#e57373',
    unpaidLeave: '#ffab91',
    sickLeave: '#e6d02d',
    workingDay: '#81c784',
  },
});

export default theme;

import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '../../../utils/testing';
import Component from '..';

describe('components | ConfirmAccount | index', () => {
  it('should render component', () => {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(script);
    const div = document.createElement('div');
    const { container } = render(<Component />, div);
    expect(container).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });
});

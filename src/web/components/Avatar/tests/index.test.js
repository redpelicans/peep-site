import React from 'react';
import { render } from '../../../utils/testing';
import Component, { UserAvatar } from '../';

const user = {
  email: 'admin@gmail.com',
  firstname: 'Amir',
  fullname: 'Amir GH',
  id: '5f204f35a04ec151e7d25f65',
  lastname: 'GH',
  roles: ['admin'],
  status: 'active',
  teams: null,
  __typename: 'User',
};

describe('components | Avatar | index', () => {
  it('should render component', () => {
    const { container } = render(<Component name="x" />);
    expect(container).toMatchSnapshot();
  });
});

describe('components | UserAvatar | index', () => {
  it('should render component', () => {
    const { container } = render(<UserAvatar user={user} />);
    expect(container).toMatchSnapshot();
  });
});

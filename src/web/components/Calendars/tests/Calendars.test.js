import React from 'react';
import endOfYear from 'date-fns/endOfYear';
import startOfYear from 'date-fns/startOfYear';
import { render, waitFor } from '../../../utils/testing';
import Component from '../';
import { GET_TEAMS } from '../hooks';
import { GET_ALL_EVENTS } from '../../Calendar/hooks';
import { GET_STATS } from '../../CompanyStats/hooks';
import { convertDateToUTC } from '../../../utils/date';

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
global.Date.now = Date;
global.Date.UTC = Date;

jest.mock('../../Calendar/hooks', () => ({
  ...jest.requireActual('../../Calendar/hooks'),
  useCurrentMonth: () => jest.fn(() => 9987907907907),
}));

describe('components | Calendars | index', () => {
  const eventsMock = {
    request: {
      query: GET_ALL_EVENTS,
      variables: {
        teamId: '1',
        from: mockedDate,
        to: mockedDate,
      },
    },
    result: () => {
      return {
        data: {},
      };
    },
  };

  const companyStatsMock = {
    request: {
      query: GET_STATS,
      variables: {
        teamId:'1',
        from:convertDateToUTC(startOfYear(new Date())),
        to:convertDateToUTC(endOfYear(new Date())),
      },
    },
    result: () => { return { 
      data: {
        companyWorkingDaysStats: [] 
      }
    } 
    },
  };

  const teamsMock = {
    request: {
      query: GET_TEAMS,
      variables: { orderBy: 'name_ASC' },
    },
    result: () => {
      return {
        data: {
          items: [
            {
              id: '1',
              name: 'team1',
              description: 'description1',
              country: 'TU',
              type: 'company',
              members: [
                {
                  activityPeriods: [],
                  person: {
                    email: 'user.usccer@user.com',
                    firstname: 'CC',
                    fullname: 'CC BBB',
                    id: '1',
                    lastname: 'BBB',
                  },
                  role: 'guest',
                },
              ],
            },
            {
              id: '2',
              name: 'team2',
              description: 'description2',
              country: 'FA',
              type: 'company',
              members: [
                {
                  activityPeriods: [],
                  person: {
                    email: 'user2.usccer@user.com',
                    firstname: 'CCa',
                    fullname: 'CCa BBB',
                    id: '2',
                    lastname: 'BBB',
                  },
                  role: 'guest',
                },
              ],
            },
          ],
        },
      };
    },
  };

  it('should render component', async () => {
    const mocks = [teamsMock, teamsMock, eventsMock, eventsMock, eventsMock, eventsMock, companyStatsMock, companyStatsMock  ];
    const { container } = render(<Component />, { mocks });
    await waitFor(() => expect(container).toMatchSnapshot());
  });
});

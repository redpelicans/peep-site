import { defineMessages } from 'react-intl';

export default defineMessages({
  workingDaysTooltipLabel: {
    id: 'components.Timeline.workingDaysTooltipLabel',
    defaultMessage: 'Total working days',
  },
});

import React from 'react';
import { render } from '../../../utils/testing';
import Component from '..';
import { Grid } from '@material-ui/core';
import { getTime } from 'date-fns';

describe('components | Timeline | index', () => {
  it('should render component', () => {
    const month = getTime(new Date());
    const spareDays = [];
    const resourceAvatar = () => <Grid></Grid>;
    const events = [];
    const team = { id: 'teamId', members: [{ person: { id: '1' }, role: 'worker' }] };
    const { container } = render(
      <Component events={events} resourceAvatar={resourceAvatar} month={month} spareDays={spareDays} team={team} />,
    );
    expect(container).toMatchSnapshot();
  });
});

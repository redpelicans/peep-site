import {
  getPersonWorkingDaysNumber,
  getMonthWorkingDaysNumber,
  isSpareDay,
  isDayOff,
  getEventWorkingDaysNumber,
  intervalToHalfDays,
} from '../utils';
import { endOfDay } from 'date-fns';
import { TYPE } from '../../../../lib/models/events';
import { TYPE as COMPANYTYPE } from '../../../../lib/models/teams';

describe('components | Timeline | utils', () => {
  const members = [
    {
      person: {
        id: '1',
      },
    },

    {
      person: {
        id: '2',
      },
    },
  ];

  const companyTeam = { type: COMPANYTYPE.company };
  const projectTeam = { type: COMPANYTYPE.project };
  const companyTeamEvents = [
    {
      id: '1',
      from: new Date(2020, 5, 15),
      to: endOfDay(new Date(2020, 5, 17)),
      person: { id: '1' },
      type: TYPE.vacation,
    },
  ];
  const projectTeamEvents = [
    {
      id: '1',
      from: new Date(2020, 5, 15),
      to: endOfDay(new Date(2020, 5, 17)),
      person: { id: '1' },
      type: TYPE.workingDay,
    },
  ];
  const month = new Date(2020, 5, 2);
  const event = { id: '1', from: new Date(2020, 5, 15), to: endOfDay(new Date(2020, 5, 17)), person: { id: '1' } };
  const spareDays = [{ from: new Date(2020, 5, 10) }];
  const personId = '1';

  it('should check if a day is a spare day', () => {
    expect(isSpareDay(new Date(2020, 5, 10), spareDays)).toBeTruthy();
    expect(isSpareDay(new Date(2020, 5, 13), spareDays)).toBeFalsy();
  });
  it('should check if a day is a spare day or a weekend', () => {
    expect(isDayOff(new Date(2020, 5, 10), spareDays)).toBeTruthy();
    expect(isDayOff(new Date(2020, 5, 7), spareDays)).toBeTruthy();
    expect(isDayOff(new Date(2020, 5, 11), spareDays)).toBeFalsy();
  });
  it('should get a month working days number', () => {
    expect(getMonthWorkingDaysNumber(month, spareDays)).toEqual(21);
  });
  it('should get an event working days number', () => {
    expect(getEventWorkingDaysNumber(event, spareDays, personId, members)).toEqual(3);
  });
  it('should get a person working days number', () => {
    expect(getPersonWorkingDaysNumber(personId, companyTeamEvents, month, spareDays, members, companyTeam)).toEqual(18);
    expect(getPersonWorkingDaysNumber(personId, projectTeamEvents, month, spareDays, members, projectTeam)).toEqual(3);
  });
  it('should return halfDays of interval', () => {
    expect(intervalToHalfDays(new Date(2020, 5, 10), new Date(2020, 5, 11, 23, 59, 59, 59))).toEqual([
      new Date(2020, 5, 10),
      new Date(2020, 5, 10, 12),
      new Date(2020, 5, 11),
      new Date(2020, 5, 11, 12),
    ]);
    expect(intervalToHalfDays(new Date(2020, 5, 10), new Date(2020, 5, 10, 11, 59, 59, 59))).toEqual([
      new Date(2020, 5, 10),
    ]);
    expect(intervalToHalfDays(new Date(2020, 5, 10, 12), new Date(2020, 5, 10, 23, 59, 59, 59))).toEqual([
      new Date(2020, 5, 10, 12),
    ]);
  });
});

import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../';

describe('components | AlertDialog | goBack', () => {
  const props = {
    open: false,
    onClose: jest.fn(),
    onConfirm: jest.fn(),
  };
  it('should render component', () => {
    const { container } = render(<Component {...props} />);
    expect(container).toMatchSnapshot();
  });
});

import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../Edit';
import { GET_PERIOD_SPARE_DAYS, GET_SPARE_DAY } from '../hooks';
import '@testing-library/jest-dom'; // to use toHaveTextContent

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useParams: () => ({
    id: '5ee767fb4f153a3541c3a10e',
  }),
}));

jest.mock('../../../hooks/user', () => ({
  useLoggedUser: () => [{ id: '5ee767fb4f153a3541c3a10e' }, false],
}));

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
global.Date.now = Date;
global.Date.UTC = Date;

const spareDayExemple = {
  id: '000a',
  from: mockedDate,
  label: 'azerty',
  country: 'FR',
  team: {
    id: '55',
    name: 'team1',
    country: 'FR',
  },
};

const spareDaysMock = {
  request: {
    query: GET_PERIOD_SPARE_DAYS,
    variables: { filter: { from: mockedDate, to: mockedDate, teamId: '55' } },
  },
  result: {
    data: {
      allSpareDays: [{ id: 1, from: new Date(), label: 'date1' }],
    },
  },
};

const spareDayByIDMock = {
  request: {
    query: GET_SPARE_DAY,
    variables: { id: '5ee767fb4f153a3541c3a10e' },
  },
  result: {
    data: {
      SpareDay: spareDayExemple,
    },
  },
};

const spareDayByID_ErrorMock = {
  request: {
    query: GET_SPARE_DAY,
    variables: { id: '5ee767fb4f153a3541c3a10e' },
  },
  error: new Error('spareDay to edit Not Found'),
};

describe('components | SpareDays | Edit', () => {
  it('should load', async () => {
    const mocks = [spareDayByIDMock, spareDayByIDMock, spareDaysMock, spareDaysMock];
    const { findByTestId } = render(<Component />, { mocks });
    await findByTestId('spareday.edit.submit');
  });

  it('should render an error', async () => {
    const mocks = [spareDayByID_ErrorMock];
    const { findAllByRole } = render(<Component />, { mocks });
    await findAllByRole('alert');
  });

  it('should render calendar with initial values', async () => {
    // const dateInLettres = format(convertDateFromUTC(new Date(spareDayExemple.from)), 'dMMMM yyyy');
    const mocks = [spareDayByIDMock, spareDaysMock];
    const { findByTestId, getByDisplayValue } = render(<Component />, { mocks });

    await findByTestId('spareday.edit.form');
    await findByTestId('spareday.edit.input.label');
    await findByTestId('spareday.datepicker');
    await findByTestId('spareday.edit.submit');

    expect(getByDisplayValue(spareDayExemple.label)).not.toBeUndefined();
    // expect(getByTestId('spareday.datepicker')).toHaveTextContent(dateInLettres);
  });
});

import React from 'react';
import { render, waitFor } from '../../../utils/testing';
import Component from '..';
import { GET_SPARE_DAYS } from '../hooks';
import { FRANCE_CODE } from '../../Teams/utils';

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
global.Date.now = Date;
global.Date.UTC = Date;

describe('components | spareday | index', () => {
  const spareDaysMock = {
    request: {
      query: GET_SPARE_DAYS,
      variables: { orderBy: 'from_ASC', filter: { from: mockedDate, to: mockedDate } },
    },
    result: {
      data: {
        items: [
          {
            id: '1',
            from: new Date(),
            label: 'test1',
            country: 'FR',
          },
        ],
        spareDaysCountries: [FRANCE_CODE],
        spareDaysYears: ['2017'],
      },
    },
  };

  it('should render component', async () => {
    const mocks = [spareDaysMock, spareDaysMock];
    const { container } = render(<Component />, { mocks });
    await waitFor(() => expect(container).toMatchSnapshot());
  });
});

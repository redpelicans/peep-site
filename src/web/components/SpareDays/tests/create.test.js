import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../Create';
import { GET_PERIOD_SPARE_DAYS } from '../hooks';
import { startOfMonth, endOfMonth } from 'date-fns';
import { GET_TEAM } from '../../../../lib/api/team';

const teamsMock = {
  request: {
    query: GET_TEAM,
  },
  result: {
    data: {
      allTeams: [
        {
          id: '5efb0025a249331949648125',
          name: 'Redp',
          country: 'FR',
          __typename: 'Team',
        },
      ],
    },
  },
};

const spareDaysMock = {
  request: {
    query: GET_PERIOD_SPARE_DAYS,
  },
  result: {
    data: {
      allSpareDays: [
        {
          id: '000a',
          from: '2020-01-04T23:00:00.000Z',
          label: 'azerty',
          country: 'FR',
          team: {
            id: '55',
            name: 'team1',
            country: 'FR',
          },
          __typename: 'SpareDay',
        },
      ],
    },
  },
};

describe('components | SpareDays | Create', () => {
  it.skip('should load', async () => {
    const mocks = [spareDaysMock, teamsMock];
    const { findByTestId } = render(<Component />, { mocks });
    await findByTestId('spareday.create.submit');
  });

  it.skip('should render an error', async () => {
    const spareDaysMock = {
      request: {
        query: GET_PERIOD_SPARE_DAYS,
        variables: { filter: { from: startOfMonth(new Date()), to: endOfMonth(new Date()) } },
      },
      error: new Error('Network Error'),
    };
    const mocks = [spareDaysMock];

    const { findAllByRole } = render(<Component />, { mocks });
    await findAllByRole('alert');
  });

  it.skip('should render calendar', async () => {
    const spareDaysMock = {
      request: {
        query: GET_PERIOD_SPARE_DAYS,
        variables: { filter: { from: startOfMonth(new Date()), to: endOfMonth(new Date()) } },
      },
      result: {
        data: {
          allSpareDays: [{ id: 1, from: new Date().toJSON(), label: 'date1' }],
        },
      },
    };
    const mocks = [spareDaysMock];
    const { container, findByTestId } = render(<Component />, { mocks });

    await findByTestId('spareday.datepicker');
    await findByTestId('spareday.create.input.label');
    await findByTestId('spareday.create.submit');
    expect(container).toMatchSnapshot();
  });
});

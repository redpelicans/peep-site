import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../List';
import { GET_SPARE_DAYS } from '../hooks';
import { FRANCE_CODE } from '../../Teams/utils';

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
global.Date.now = Date;
global.Date.UTC = Date;

jest.mock('../../../hooks/user', () => ({
  useLoggedUser: () => [{ id: '5ee767fb4f153a3541c3a10e' }, false],
}));

describe('components | SpareDays | List', () => {
  const spareDaysMock = {
    request: {
      query: GET_SPARE_DAYS,
      variables: { orderBy: 'from_ASC', filter: { from: mockedDate, to: mockedDate } },
    },
    result: {
      data: {
        items: [
          {
            id: 1,
            from: mockedDate,
            label: 'date1',
            country: 'FR',
            team: {
              id: '55',
              name: 'team1',
              country: 'FR',
            },
          },
          {
            id: 2,
            from: mockedDate,
            label: 'date2',
            country: 'FR',
            team: {
              id: '55',
              name: 'team1',
              country: 'FR',
            },
          },
        ],
        spareDaysCountries: [FRANCE_CODE],
        spareDaysYears: ['2017'],
      },
    },
  };

  it('should load', async () => {
    const mocks = [spareDaysMock, spareDaysMock];
    const { findByTestId } = render(<Component />, { mocks });
    await findByTestId('spareday.list');
  });

  it('should render spare days', async () => {
    const mocks = [spareDaysMock, spareDaysMock];
    const { container, findAllByTestId } = render(<Component />, { mocks });
    const res = await findAllByTestId(/spareday#/);
    expect(res.length).toEqual(2);
    expect(container).toMatchSnapshot();
  });

  it('should render an error', async () => {
    const spareDaysMock = {
      request: {
        query: GET_SPARE_DAYS,
        variables: { orderBy: 'from_ASC', filter: { from: mockedDate, to: mockedDate } },
      },
      error: new Error('Network Error'),
    };
    const mocks = [spareDaysMock];
    const { findAllByRole } = render(<Component />, { mocks });

    await findAllByRole('alert');
  });
});

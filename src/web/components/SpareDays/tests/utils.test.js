import { codeToCodeAndName, sortCountriesByName } from '../utils';
import { FRANCE_CODE } from '../../Teams/utils';
import { BELGIUM_CODE, SWITZERLAND_CODE, TUNISIA_CODE } from '../../../../lib/models/teams';

describe('components | SpareDays | utils', () => {
  const code = FRANCE_CODE;
  const codes = [TUNISIA_CODE, FRANCE_CODE, BELGIUM_CODE, SWITZERLAND_CODE];
  it('should convert code to code and name', () => {
    expect(codeToCodeAndName(code)).toEqual({ code: 'FR', name: 'France' });
  });
  it('should sort countries by name', () => {
    expect(sortCountriesByName(codes)).toEqual([BELGIUM_CODE, FRANCE_CODE, SWITZERLAND_CODE, TUNISIA_CODE]);
  });
});

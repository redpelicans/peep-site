import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../';

describe('components | LoadingProvider | index', () => {
  it('should render component', () => {
    const { container } = render(
      <Component>
        <span>a</span>
      </Component>,
    );
    expect(container).toMatchSnapshot();
  });
});

import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../';

describe('components | NavTitle | index', () => {
  it('should render component', () => {
    const { container } = render(<Component message="message" loading={true} />);
    expect(container).toMatchSnapshot();
  });
});

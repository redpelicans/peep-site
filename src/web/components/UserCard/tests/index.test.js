import React from 'react';
import { render, waitFor } from '../../../utils/testing';
import Component from '..';

describe('components | UserCard | index', () => {
  it('should render component', async () => {
    const user = { firstname: 'amine', lastname: 'daoussi' };
    const { container } = render(<Component user={user} />);
    await waitFor(() => expect(container).toMatchSnapshot());
  });
});

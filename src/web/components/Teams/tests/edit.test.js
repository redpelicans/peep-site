import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../Edit';
import { GET_TEAM_BY_ID, SEARCH_USERS } from '../hooks';
import { GET_ALL_USERS } from '../../Users/hooks';

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useParams: () => ({
    id: 'teamId',
  }),
}));

describe('components | Team | Edit', () => {
  const filteredUsersMock = {
    request: {
      query: SEARCH_USERS,
      variables: { match: 'test' },
    },
    result: {
      data: {
        searchUsers: [],
      },
    },
  };
  const allUsersMock = {
    request: {
      query: GET_ALL_USERS,
      variables: {},
    },
    result: {
      data: {
        allUsers: [],
      },
    },
  };
  const teamByIDMock = {
    request: {
      query: GET_TEAM_BY_ID,
      variables: { id: 'teamId' },
    },
    result: () => {
      return {
        data: {
          Team: {
            id: 'teamId',
            name: 'meeedbatesta',
            description: '<p>akjakj</p>↵',
            country: 'FR',
            type: 'company',
            members: [
              {
                person: {
                  id: 'person',
                  firstname: 'firstname',
                  lastname: 'lastname',
                  fullname: 'fullname',
                  email: 'email',
                },
                role: 'worker',
                activityPeriods: [],
              },
            ],
          },
        },
      };
    },
  };

  it('should load', async () => {
    const mocks = [allUsersMock, allUsersMock, filteredUsersMock, filteredUsersMock, teamByIDMock];
    const { container, findByTestId } = render(<Component />, { mocks });
    await findByTestId('team.edit.input.name');
    await findByTestId('team.edit.button.addMembers');
    await findByTestId('team.edit.submit');
    expect(container).toMatchSnapshot();
  });

  it('should render add members form', async () => {
    const mocks = [allUsersMock, allUsersMock, teamByIDMock];
    const { container, findByTestId } = render(<Component />, { mocks });
    await findByTestId('team.edit.input.name');
    await findByTestId('team.edit.button.addMembers');
    await findByTestId('team.edit.input.person.id');
    await findByTestId('team.edit.input.person.role');
    await findByTestId('team.edit.input.person.expirationDate');
    await findByTestId('team.edit.submit');
    expect(container).toMatchSnapshot();
  });
});

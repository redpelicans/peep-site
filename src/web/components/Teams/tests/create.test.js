import React from 'react';
import { render, fireEvent } from '../../../utils/testing';
import Component from '../Create';
import { GET_ALL_USERS } from '../../Users/hooks';

describe('components | Teams | Create', () => {
  const allUsersMock = {
    request: {
      query: GET_ALL_USERS,
      variables: {},
    },
    result: {
      data: {
        allUsers: [],
      },
    },
  };

  it('should load', async () => {
    const mocks = [allUsersMock, allUsersMock];
    const { findByTestId } = render(<Component />, { mocks });
    await findByTestId('team.create.input.name');
    await findByTestId('team.create.button.addMembers');
    await findByTestId('team.create.submit');
  });

  it('should render add members form', async () => {
    const mocks = [allUsersMock, allUsersMock];
    const { findByTestId } = render(<Component />, { mocks });
    const addMemberButton = await findByTestId('team.create.button.addMembers');
    fireEvent.click(addMemberButton);
    await findByTestId('team.create.input.person.id');
    await findByTestId('team.create.input.person.role');
  });
});

import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../List';
import { GET_TEAMS } from '../hooks';
import { isActiveDay } from '../../Timeline/utils';
import { addDays } from 'date-fns';

describe('components | teams | List', () => {
  const teamsMock = {
    request: {
      query: GET_TEAMS,
      variables: { first: 20, skip: 0, orderBy: 'name_ASC' },
    },
    result: () => {
      return {
        data: {
          items: [
            {
              id: '1',
              name: 'team1',
              description: 'description1',
              country: 'TU',
              type: 'company',
              createdBy: '1',
              members: [
                {
                  activityPeriods: [],
                  person: {
                    email: 'user.usccer@user.com',
                    firstname: 'CC',
                    fullname: 'CC BBB',
                    id: '1',
                    lastname: 'BBB',
                  },
                  role: 'guest',
                },
              ],
            },
            {
              id: '2',
              name: 'team2',
              description: 'description2',
              country: 'FA',
              type: 'company',
              createdBy: '2',
              members: [
                {
                  activityPeriods: [],
                  person: {
                    email: 'user2.usccer@user.com',
                    firstname: 'CCa',
                    fullname: 'CCa BBB',
                    id: '2',
                    lastname: 'BBB',
                  },
                  role: 'guest',
                },
              ],
            },
          ],
          total: { count: 2 },
        },
      };
    },
  };

  it('should load', async () => {
    const mocks = [teamsMock, teamsMock];
    const { findByTestId } = render(<Component />, { mocks });
    await findByTestId('team.list');
  });

  it('should render teams', async () => {
    const mocks = [teamsMock, teamsMock];
    const { container, findAllByTestId } = render(<Component />, { mocks });

    const res = await findAllByTestId(/team#/);
    expect(res.length).toEqual(2);
    expect(container).toMatchSnapshot();
  });

  it('should render an error', async () => {
    const errorTeamsMock = {
      request: {
        query: GET_TEAMS,
        variables: { first: 20, skip: 0, orderBy: 'name_ASC' },
      },
      error: new Error('Network Error'),
    };
    const mocks = [teamsMock, errorTeamsMock];
    const { findAllByRole } = render(<Component />, { mocks });

    await findAllByRole('alert');
  });

  it('should check if member is active', () => {
    const firstMember = {
      activityPeriods: [{ inceptionDate: new Date(), expirationDate: null }],
      person: {
        email: 'user2.usccer@user.com',
        firstname: 'CCa',
        fullname: 'CCa BBB',
        id: '2',
        lastname: 'BBB',
      },
      role: 'guest',
    };
    const secondMember = {
      activityPeriods: [{ inceptionDate: null, expirationDate: null }],
      person: {
        email: 'user2.usccer@user.com',
        firstname: 'CCa',
        fullname: 'CCa BBB',
        id: '2',
        lastname: 'BBB',
      },
      role: 'guest',
    };

    const members = [firstMember, secondMember];
    expect(isActiveDay(firstMember.person.id, members, new Date())).toBeTruthy();
    expect(isActiveDay(secondMember.person.id, members, new Date())).toBeTruthy();
  });

  it('should check if member is inActive', () => {
    const member = {
      activityPeriods: [{ inceptionDate: addDays(new Date(), 1), expirationDate: addDays(new Date(), 3) }],
      person: {
        email: 'user2.usccer@user.com',
        firstname: 'CCa',
        fullname: 'CCa BBB',
        id: '2',
        lastname: 'BBB',
      },
      role: 'guest',
    };

    const members = [member];

    expect(isActiveDay(member.person.id, members, new Date())).toBeFalsy();
  });
});

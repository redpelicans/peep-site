import TeamCreate from './Create';
import TeamList from './List';
import TeamEdit from './Edit';

export default {
  TeamCreate,
  TeamList,
  TeamEdit,
};

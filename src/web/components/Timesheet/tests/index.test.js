import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../';
import { TYPE } from '../../../../lib/models/teams';

const date = 1596236400000;
const events = [];
const members = [
  {
    activityPeriods: null,
    person: {
      email: 'admin@gmail.com',
      firstname: 'Amir',
      fullname: 'Amir GH',
      id: '5f204f35a04ec151e7d25f65',
      lastname: 'GH',
    },
    role: 'worker',
  },
];
const spareDays = [];
const team = {
  name: 'dddddd',
  type: TYPE.project,
};

describe('components | Timesheet | index', () => {
  it('should render component', () => {
    const { container } = render(
      <Component date={date} events={events} members={members} spareDays={spareDays} team={team} />,
    );
    expect(container).toMatchSnapshot();
  });
});

import React from 'react';
export const RoutesContext = React.createContext();
export const RoutesProvider = RoutesContext.Provider;

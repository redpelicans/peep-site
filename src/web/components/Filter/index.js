import React from 'react';
import propTypes from 'prop-types';
import { fade, makeStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { Grid } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  search: {
    position: 'relative',

    borderRadius: 4,
    backgroundColor: fade('#f5f5f5', 0.8),
    '&:hover': {
      backgroundColor: fade('#f5f5f5', 1),
    },

    marginBottom: 10,
    width: '30%',
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

const Filter = ({ setSearchTerm }) => {
  const classes = useStyles();

  const handleChange = event => {
    setSearchTerm(event.target.value);
  };
  return (
    <Grid className={classes.search}>
      <Grid className={classes.searchIcon}>
        <SearchIcon />
      </Grid>
      <InputBase
        placeholder="Search…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
        onChange={handleChange}
      />
    </Grid>
  );
};
Filter.propTypes = {
  setSearchTerm: propTypes.func,
};
export default Filter;

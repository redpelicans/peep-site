import React from 'react';
import FacebookLogin from 'react-facebook-login';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  button: {
    width: '100%',
    border: 'none',
    outline: 'none',
  },
  login: {
    [theme.breakpoints.down('sm')]: {
      fontSize: theme.spacing(1.5),
    },
  },

  logoGoogle: {
    marginRight: theme.spacing(1),
    width: theme.spacing(4),
    height: 'auto',
  },
  fbBtn: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
    height: '50px',
    background: '#3b5998',
    border: `1px solid ${theme.palette.grey[400]}`,
    borderRadius: theme.spacing(1),
    justifyContent: 'center',
    cursor: 'pointer',
    color: '#FFF',
    '& .fa-facebook-square': {
      marginRight: theme.spacing(2),
      fontSize: '26px',
    },
    '&:hover': {
      background: '#394b70',
    },
    '&:focus': {
      outline: 'none',
    },
  },
}));

const LoginWithFacebook = ({ responseFacebook, buttonText }) => {
  const classes = useStyles();
  return (
    <FacebookLogin
      appId="803487910398698"
      fields="name,picture,email"
      icon="fab fa-facebook-square"
      version="7.0"
      disableMobileRedirect
      size="small"
      textButton={<Typography>{buttonText}</Typography>}
      callback={responseFacebook}
      cssClass={classes.fbBtn}
    />
  );
};

LoginWithFacebook.propTypes = {
  responseFacebook: PropTypes.func,
  buttonText: PropTypes.object,
};

export default LoginWithFacebook;

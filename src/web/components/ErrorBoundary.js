import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bomb: {
    fontSize: '10em',
  },
};

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    console.error(error, info); // eslint-disable-line no-console
  }

  render() {
    if (this.state.hasError) {
      return (
        <div className={this.props.classes.container}>
          <Typography variant="body2" gutterBottom>
            <SentimentVeryDissatisfiedIcon className={this.props.classes.bomb} />
            Something went wrong
          </Typography>
        </div>
      );
    }

    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object,
};

export default withStyles(styles)(ErrorBoundary);

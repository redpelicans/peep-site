import React from 'react';
import { render } from '../../../utils/testing';
import Component from '..';

const notification = {
  content: 'undefined',
  level: 'success',
  title: 'Spare day cd created',
};
describe('components | Notification | index', () => {
  it('should render component', () => {
    const { container } = render(<Component notification={notification} />);
    expect(container).toMatchSnapshot();
  });
});

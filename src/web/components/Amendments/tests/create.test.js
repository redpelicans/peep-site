import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../Create';
import { GET_TEAMS } from '../../Teams/hooks';

describe('components | Amendment | Create', () => {
  const allTeamsMock = {
    request: {
      query: GET_TEAMS,
      variables: {},
    },
    result: {
      data: {
        allTeams: [],
      },
    },
  };

  it('should load', async () => {
    const mocks = [allTeamsMock, allTeamsMock];
    const { findByTestId } = render(<Component />, { mocks });
    await findByTestId('amendment.create.input.from');
    await findByTestId('amendment.create.input.to');
    await findByTestId('amendment.create.input.days');
    await findByTestId('amendment.create.input.description');
  });
});

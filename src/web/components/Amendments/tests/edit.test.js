import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../Edit';
import { GET_AMENDMENT } from '../hooks';

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useParams: () => ({
    id: '5ee767fb4f153a3541c3a10e',
  }),
}));

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
global.Date.now = Date;
global.Date.UTC = Date;

describe('components | User | Edit', () => {
  it('should load', async () => {
    const amendmentMock = {
      request: {
        query: GET_AMENDMENT,
        variables: { id: '5ee767fb4f153a3541c3a10e' },
      },
      result: {
        data: {
          Amendment: {
            id: '5ee767fb4f153a3541c3a10e',
            from: mockedDate,
            to: mockedDate,
            days: 0,
            description: 'description',
            team: { id: 'teamId', name: 'teamName' },
            person: { id: 'personId', fullname: 'person person' },
          },
        },
      },
    };

    const mocks = [amendmentMock, amendmentMock];
    const { container, findByTestId } = render(<Component />, { mocks });
    await findByTestId('amendment.edit.input.to');
    await findByTestId('amendment.edit.input.days');
    await findByTestId('amendment.edit.submit');
    expect(container).toMatchSnapshot();
  });
});

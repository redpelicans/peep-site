import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../List';
import { GET_AMENDMENTS } from '../hooks';

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
global.Date.now = Date;
global.Date.UTC = Date;

jest.mock('../../../hooks/user', () => ({
  useLoggedUser: () => [{ id: '5ee767fb4f153a3541c3a10e' }, false],
}));

describe('components | Amendments | List', () => {
  const amendmentsMock = {
    request: {
      query: GET_AMENDMENTS,
      variables: { orderBy: 'from_ASC', filter: {} },
    },
    result: {
      data: {
        items: [
          {
            id: 1,
            from: mockedDate,
            to: mockedDate,
            team: {
              id: '55',
              name: 'team1',
              country: 'FR',
            },
            person: {
              id: 'userId',
              lastname: 'user',
              firstname: 'user',
              fullname: 'user user',
              email: 'email',
            },
            days: 10,
            description: 'description',
          },
          {
            id: 2,
            from: mockedDate,
            to: mockedDate,
            team: {
              id: '55',
              name: 'team1',
              country: 'FR',
            },
            person: {
              id: 'userId2',
              lastname: 'user2',
              firstname: 'user2',
              fullname: 'user2 user2',
              email: 'email',
            },
            days: 10,
            description: 'description',
          },
        ],
        amendmentsUsers: [
          { id: 'userId2', fullname: 'user2 user2' },
          { id: 'userId', fullname: 'user user' },
        ],
        amendmentsTeams: [{ id: '55', name: 'team1' }],
      },
    },
  };

  it('should load', async () => {
    const mocks = [amendmentsMock, amendmentsMock];
    const { findByTestId } = render(<Component />, { mocks });
    await findByTestId('amendment.list');
  });

  it('should render amendments', async () => {
    const mocks = [amendmentsMock, amendmentsMock];
    const { container, findAllByTestId } = render(<Component />, { mocks });
    const res = await findAllByTestId(/amendment#/);
    expect(res.length).toEqual(2);
    expect(container).toMatchSnapshot();
  });

  it('should render an error', async () => {
    const amendmentsMock = {
      request: {
        query: GET_AMENDMENTS,
        variables: { orderBy: 'from_ASC', filter: {} },
      },
      error: new Error('Network Error'),
    };
    const mocks = [amendmentsMock];
    const { findAllByRole } = render(<Component />, { mocks });

    await findAllByRole('alert');
  });
});

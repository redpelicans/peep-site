import { map } from 'ramda';
import { convertDateFromUTC } from '../../utils/date';

export const convertAmendmentsDate = map(amendment => ({
  ...amendment,
  from: convertDateFromUTC(amendment.from),
  to: convertDateFromUTC(amendment.to),
}));

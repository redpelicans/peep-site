import { defineMessages } from 'react-intl';

export default defineMessages({
  defaultPlaceholder: {
    id: 'users.defaultPlaceholder',
    defaultMessage: 'Search...',
  },
});

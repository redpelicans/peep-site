import { renderHook } from '@testing-library/react-hooks';
import useStyles from '../styles';

describe('web :: components :: SearchBar :: styles', () => {
  it('should match snapshot', () => {
    const { result } = renderHook(() => useStyles());
    expect(result.current).toMatchSnapshot();
  });
});

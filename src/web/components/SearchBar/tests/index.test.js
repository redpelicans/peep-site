import React from 'react';
import { render } from '../../../utils/testing';
import SearchBar from '..';

describe('components | SearchBar', () => {
  it('should match snapshot', () => {
    const onChange = jest.fn();
    const { container } = render(<SearchBar onChange={onChange} />);
    expect(container).toMatchSnapshot();
  });
});

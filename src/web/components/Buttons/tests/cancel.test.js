import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../cancel';

describe('components | Buttons | cancel', () => {
  it('should render component', () => {
    const { container } = render(<Component />);
    expect(container).toMatchSnapshot();
  });
});

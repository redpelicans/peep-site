import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../goBack';

describe('components | Buttons | goBack', () => {
  it('should render component', () => {
    const { container } = render(<Component />);
    expect(container).toMatchSnapshot();
  });
});

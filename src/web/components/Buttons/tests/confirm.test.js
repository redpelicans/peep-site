import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../confirm';

describe('components | Buttons | confirm', () => {
  it('should render component', () => {
    const { container } = render(<Component />);
    expect(container).toMatchSnapshot();
  });
});

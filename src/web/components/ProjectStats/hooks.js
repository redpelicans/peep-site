import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';

export const GET_STATS = gql`
  query ProjectStats($teamId: ObjectID!, $from: DateTime!, $to: DateTime!) {
    projectWorkingDaysStats(teamId: $teamId, from: $from, to: $to) {
      worker {
        id
        fullname
        email
        firstname
        lastname
      }
      lastAmendment {
        id
        from
        to
        days
      }
      amendmentWorkingDays
      currentYearWorkingDays
    }
  }
`;

export const useProjectStats = (params, options) => {
  const res = useQuery(GET_STATS, {
    ...options,
    fetchPolicy: 'cache-and-network',
    variables: params,
  });
  return [res.data?.projectWorkingDaysStats || [], res.loading, res.error];
};

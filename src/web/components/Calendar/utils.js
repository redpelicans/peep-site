import { convertDateFromUTC } from '../SpareDays/utils';
import { map } from 'ramda';
export const convertSpareDaysFromUTC = map(spareDay => ({ ...spareDay, from: convertDateFromUTC(spareDay.from) }));

import { getTime, addMonths, subMonths, startOfMonth } from 'date-fns';

export const moveNextMonth = currentMonth => getTime(addMonths(currentMonth, 1));
export const movePreviousMonth = currentMonth => getTime(subMonths(currentMonth, 1));
export const moveCurrentMonth = () => getTime(startOfMonth(new Date()));

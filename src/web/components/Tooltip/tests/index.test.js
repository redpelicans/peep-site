import React from 'react';
import { render } from '../../../utils/testing';
import Component from '..';

describe('components | Tooltip | index', () => {
  it('should render component', () => {
    const { container } = render(<Component data={{ label: 'a' }} />);
    expect(container).toMatchSnapshot();
  });
});

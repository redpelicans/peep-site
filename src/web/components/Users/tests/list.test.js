import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../List';
import { GET_USERS_PAGE } from '../hooks';
import { RIGHT } from '../../../roles';

const mockedDate = new Date(2017, 11, 10);
global.Date = jest.fn(() => mockedDate);
global.Date.now = Date;
global.Date.UTC = Date;

describe('components | Users | List', () => {
  const usersMock = {
    request: {
      query: GET_USERS_PAGE,
      variables: { first: 20, skip: 0, orderBy: 'firstname_ASC' },
      fetchPolicy: 'cache-and-network',
    },
    result: {
      data: {
        items: [
          {
            id: '1',
            firstname: 'firstname1',
            lastname: 'lastname1',
            fullname: 'firstname1 lastname1',
            email: 'email@email.email1',
            status: 'pending',
            roles: ['admin', 'user'],
            rights: [RIGHT.spareDays],
            teams: [{ id: '1', name: 'teamName1', description: 'teamDescription1', type: 'company' }],
            lastLogin: new Date(),
          },
          {
            id: '2',
            firstname: 'firstname2',
            lastname: 'lastname2',
            fullname: 'firstname2 lastname2',
            email: 'email@email.email2',
            status: 'pending',
            roles: ['user'],
            rights: [RIGHT.spareDays],
            teams: [{ id: '2', name: 'teamName2', description: 'teamDescription2', type: 'company' }],
            lastLogin: new Date(),
          },
        ],
        total: { count: 2 },
      },
    },
  };

  it('should load', async () => {
    const mocks = [usersMock, usersMock];
    const { findByTestId } = render(<Component />, { mocks });
    await findByTestId('user.list');
  });

  it('should render users', async () => {
    const mocks = [usersMock, usersMock];
    const { container, findAllByTestId } = render(<Component />, { mocks });
    const res = await findAllByTestId(/user#/);
    expect(res.length).toEqual(2);
    expect(container).toMatchSnapshot();
  });

  it('should render an error', async () => {
    const usersMock = {
      request: {
        query: GET_USERS_PAGE,
        variables: { first: 20, skip: 0, orderBy: 'firstname_ASC' },
      },
      error: new Error('Network Error'),
    };
    const mocks = [usersMock, usersMock];
    const { findAllByRole } = render(<Component />, { mocks });

    await findAllByRole('alert');
  });
});

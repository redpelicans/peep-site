import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../Create';
import { GET_USER } from '../../../hooks/user';
import { ROLE } from '../../../../lib/models/user';
import { RIGHT } from '../../../roles';

describe('components | Users | Create', () => {
  it('should load and render component', async () => {
    const userMock = {
      request: {
        query: GET_USER,
        skip: true,
        variables: { loggedUserId: '5eb27ce9bca5fc3dd67a31b0' },
      },
      result: {
        data: {
          User: {
            email: 'mohamed.benhnia@yahoo.com',
            firstname: 'MOHAMED',
            fullname: 'MOHAMED BEN HNIA',
            id: '5eb27ce9bca5fc3dd67a31b0',
            lastname: 'BEN HNIA',
            roles: [ROLE.admin, ROLE.user],
            rights: [RIGHT.spareDays],
            status: 'active',
            teams: [
              {
                description: '<p>akjakj</p>↵',
                id: '5ec565f63951e254243ff951',
                members: [],
                name: 'meeedbatesta',
                timezone: null,
              },
            ],
          },
        },
      },
    };
    const mocks = [userMock];
    const { container, findByTestId } = render(<Component />, { mocks });
    await findByTestId('user.create.input.firstname');
    await findByTestId('user.create.input.lastname');
    await findByTestId('user.create.input.email');
    await findByTestId('user.create.input.roles');
    await findByTestId('user.create.input.status');
    await findByTestId('user.create.submit');
    expect(container).toMatchSnapshot();
  });
});

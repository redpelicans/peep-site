import React from 'react';
import { render } from '../../../utils/testing';
import Component from '../Edit';
import { GET_USER_BY_ID } from '../hooks';
import { GET_USER_TEAMS } from '../../Teams/hooks';
import { RIGHT } from '../../../roles';

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useParams: () => ({
    id: '5ee767fb4f153a3541c3a10e',
  }),
}));

jest.mock('../../../hooks/user', () => ({
  useLoggedUser: () => [{ id: '5ee767fb4f153a3541c3a10e' }, false],
}));

describe('components | User | Edit', () => {
  it('should load', async () => {
    const allTeamsMock = {
      request: {
        query: GET_USER_TEAMS,
        variables: { filter: { userId: '5ee767fb4f153a3541c3a10e' } },
      },
      result: {
        data: {
          allTeams: [],
        },
      },
    };

    const userByIDMock = {
      request: {
        query: GET_USER_BY_ID,
        variables: { id: '5ee767fb4f153a3541c3a10e' },
      },
      result: {
        data: {
          User: {
            email: 'daoussiamine@darejs.com',
            firstname: 'AMINEJ',
            fullname: 'AMINEJ DAOUSSI',
            id: '5ee767fb4f153a3541c3a10e',
            lastname: 'DAOUSSI',
            roles: ['admin', 'user'],
            rights: [RIGHT.spareDays],
            status: 'pending',
            lastLogin: new Date(),
            teams: [
              {
                description: '<p><del>testAddMembershahaa</del></p>↵',
                id: '5edfc7e2482d2c3bbdd28343',
                members: [],
                name: 'newTest',
              },
            ],
          },
        },
      },
    };
    const mocks = [userByIDMock, allTeamsMock];
    const { container, findByTestId } = render(<Component />, { mocks });
    await findByTestId('user.edit.input.firstname');
    await findByTestId('user.edit.input.lastname');
    await findByTestId('user.edit.submit');
    expect(container).toMatchSnapshot();
  });
});

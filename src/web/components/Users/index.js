import UserCreate from './Create';
import UserList from './List';
import UserEdit from './Edit';

export default {
  UserCreate,
  UserList,
  UserEdit,
};

import React from 'react';
import { render, waitFor } from '../../../utils/testing';
import Component from '..';

describe('components | Layout | index', () => {
  it('should render component', async () => {
    const user = { firstname: 'amine', lastname: 'daoussi' };
    const miniDrawerOpen = true;
    const { container } = render(<Component user={user} miniDrawerOpen={miniDrawerOpen} />);
    await waitFor(() => expect(container).toMatchSnapshot());
  });
});

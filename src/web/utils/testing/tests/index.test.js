import React from 'react';
import { render } from '..';

describe('app | components | TestProvider | index', () => {
  it('should be defined', () => {
    const { container } = render(<div />);
    expect(container).toBeDefined();
  });
});

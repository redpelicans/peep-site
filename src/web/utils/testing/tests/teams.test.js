import { atLeastManager, isManagerOrheadWorker } from '../..';
import { ROLE } from '../../../../lib/models/user';

const userManager = {
  id: 'user1',

  roles: ['user'],

  teams: [
    {
      id: '5f1a22aa4001aa52c2bb2bb1',

      members: [
        {
          person: {
            id: 'user2',
          },
          role: 'worker',
        },
        {
          person: {
            id: 'user1',
          },
          role: 'manager',
        },
      ],
    },
  ],
};

const userWorker = {
  id: 'user2',

  roles: ['user'],

  teams: [
    {
      id: 'team',

      members: [
        {
          person: {
            id: 'userManager',
          },
          role: 'manager',
        },

        {
          person: {
            id: 'user2',
          },
          role: 'worker',
        },
      ],
    },
  ],
};

const team = {
  id: 'team1',

  members: [
    {
      person: {
        id: 'user1',
      },
      role: 'manager',
    },
    {
      person: {
        id: 'user2',
      },
      role: 'worker',
    },
  ],
};

const teamWithoutMembers = {
  id: 'team2',
  members: null,
};

const admin = {
  id: 'admin',
  roles: [ROLE.admin],
};

describe('Teams', () => {
  it('should check if user is  manager or  headWorker ', () => {
    expect(atLeastManager(userManager)).toBeTruthy();
    expect(atLeastManager(admin)).toBeTruthy();
  });
  it('should check if user is not manager or headWorker', () => {
    expect(atLeastManager(userWorker)).toBeFalsy();
  });

  it('should check if user is manager in team', () => {
    expect(isManagerOrheadWorker(userManager, team)).toBeTruthy();
  });
  it('should check if user is not manager or headWorker in team', () => {
    expect(isManagerOrheadWorker(userWorker, team)).toBeFalsy();
  });
  it('should return false if team.members is null', () => {
    expect(isManagerOrheadWorker(userWorker, teamWithoutMembers)).toBeFalsy();
  });
});

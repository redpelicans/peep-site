import React from 'react';
import { render } from '@testing-library/react';
import { createGenerateClassName, StylesProvider, ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { MockedProvider } from '@apollo/client/testing';
import { RoutesProvider } from '../../components/RoutesProvider';
import routes from '../../routes';
import { AuthProvider } from '../../components/AuthProvider';
import authManager from '../../authManager';
import LanguageProvider from '../../components/LanguageProvider';
import NavigationProvider from '../../components/NavigationProvider';
import NotificationProvider from '../../components/NotificationProvider';
import LoadingProvider from '../../components/LoadingProvider';
import CurrentMonthProvider from '../../components/CurrentMonthProvider';

const theme = createMuiTheme({});

const generateClassName = createGenerateClassName({ disableGlobal: true });
const history = createBrowserHistory();
const defaultClient = {
  writeData: new Function(),
  resetStore: new Function(),
};

const AllProvidersMaker = ({ mocks = [], client = defaultClient } = {}) => ({ children }) => ( // eslint-disable-line
  <StylesProvider generateClassName={generateClassName} injectFirst>
    <MockedProvider mocks={mocks} addTypename={false}>
      <AuthProvider value={authManager(client)}>
        <NavigationProvider>
          <NotificationProvider>
            <LoadingProvider>
                  <CurrentMonthProvider>
              <RoutesProvider value={routes}>
                <LanguageProvider messages={window.I18N || {}}>
                  <ThemeProvider theme={theme}>
                    <Router history={history}>{children}</Router>
                  </ThemeProvider>
                </LanguageProvider>
              </RoutesProvider>
                </CurrentMonthProvider>
            </LoadingProvider>
          </NotificationProvider>
        </NavigationProvider>
      </AuthProvider>
    </MockedProvider>
  </StylesProvider>
);

export const customRender = (ui, options) => render(ui, { wrapper: AllProvidersMaker(options), options });
export * from '@testing-library/react';
export { customRender as render };

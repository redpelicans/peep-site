import { atLeastManager, isManagerOrheadWorker, sortMembersByFirstnameCaseInsensitive } from '..';
import { ROLE } from '../../../lib/models/user';

const userManager = {
  id: 'user1',

  roles: ['user'],

  teams: [
    {
      id: '5f1a22aa4001aa52c2bb2bb1',

      members: [
        {
          person: {
            id: 'user2',
          },
          role: 'worker',
        },
        {
          person: {
            id: 'user1',
          },
          role: 'manager',
        },
      ],
    },
  ],
};

const userHeadWorker = {
  id: 'user1',

  roles: ['user'],

  teams: [
    {
      id: '5f1a22aa4001aa52c2bb2bb1',

      members: [
        {
          person: {
            id: 'user2',
          },
          role: 'worker',
        },
        {
          person: {
            id: 'user1',
          },
          role: 'headWorker',
        },
      ],
    },
  ],
};

const userWorker = {
  id: 'user2',

  roles: ['user'],

  teams: [
    {
      id: 'team',

      members: [
        {
          person: {
            id: 'userManager',
          },
          role: 'manager',
        },

        {
          person: {
            id: 'user2',
          },
          role: 'worker',
        },
      ],
    },
  ],
};

const userWithoutTeams = {
  id: 'user',
  roles: ['user'],
  teams: [],
};
const userWithoutTeamsAttribut = {
  id: 'user',
  roles: ['user'],
};

const userWithTeamWithoutMembers = {
  id: 'user',
  roles: ['user'],
  teams: [teamWithoutMembers],
};

const team = {
  id: 'team1',

  members: [
    {
      person: {
        id: 'user1',
      },
      role: 'manager',
    },
    {
      person: {
        id: 'user2',
      },
      role: 'worker',
    },
  ],
};

const teamWithoutMembers = {
  id: 'team2',
  members: null,
};

const admin = {
  id: 'admin',
  roles: [ROLE.admin],
};

const members = [
  {
    person: {
      firstname: 'Samer',
    },
  },
  {
    person: {
      firstname: 'Amir',
    },
  },
  {
    person: {
      firstname: 'Mohammed',
    },
  },
  {
    person: {
      firstname: 'Amine',
    },
  },
];

describe('utils | index', () => {
  it('should check if user is manager in any team ', () => {
    expect(atLeastManager(userManager)).toBeTruthy();
    expect(atLeastManager(userHeadWorker)).toBeTruthy();
    expect(atLeastManager(userWorker)).toBeFalsy();
    expect(atLeastManager(userWithoutTeams)).toBeFalsy();
    expect(atLeastManager(userWithoutTeamsAttribut)).toBeFalsy();
    expect(atLeastManager(userWithTeamWithoutMembers)).toBeFalsy();
    expect(atLeastManager(admin)).toBeTruthy();
    expect(atLeastManager(undefined)).toBeFalsy();
  });

  it('should check if user is manager or headWorker in a team', () => {
    expect(isManagerOrheadWorker(userManager, team)).toBeTruthy();
    expect(isManagerOrheadWorker(userWorker, team)).toBeFalsy();
    expect(isManagerOrheadWorker(userManager, teamWithoutMembers)).toBeFalsy();
    expect(isManagerOrheadWorker(userWithoutTeams, team)).toBeFalsy();
    expect(isManagerOrheadWorker(userWithTeamWithoutMembers, team)).toBeFalsy();
    expect(isManagerOrheadWorker(undefined, team)).toBeFalsy();
  });

  it('should sort members by firstname', () => {
    expect(sortMembersByFirstnameCaseInsensitive(members)).toEqual([
      {
        person: {
          firstname: 'Amine',
        },
      },
      {
        person: {
          firstname: 'Amir',
        },
      },
      {
        person: {
          firstname: 'Mohammed',
        },
      },
      {
        person: {
          firstname: 'Samer',
        },
      },
    ]);
  });
});

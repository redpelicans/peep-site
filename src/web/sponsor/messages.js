import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'sponsor.from',
    defaultMessage: 'Peep by {name}',
  },
});

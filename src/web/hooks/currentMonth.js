import { useContext } from 'react';
import { CurrentMonthContext } from '../components/CurrentMonthProvider';

export default () => useContext(CurrentMonthContext);

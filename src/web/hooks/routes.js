import { useContext } from 'react';
import { RoutesContext } from '../components/RoutesProvider';

export default () => useContext(RoutesContext);

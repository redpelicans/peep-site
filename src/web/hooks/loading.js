import { useContext } from 'react';
import { LoadingContext } from '../components/LoadingProvider';

export default () => useContext(LoadingContext);

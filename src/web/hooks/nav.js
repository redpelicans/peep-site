import { useContext } from 'react';
import { NavContext } from '../components/NavProvider';
export default () => useContext(NavContext);

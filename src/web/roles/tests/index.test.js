import {
  hasSpareDaysRight,
  RIGHT,
  ROLE,
  isAdmin,
  isLoggedUser,
  isAdminOrLoggedUser,
  hasTeamsCreateRightOrManager,
  isAdminOrManagerInTeam,
  isManagerOfUser,
  workTogether,
  isMember,
} from '..';

describe('web | roles', () => {
  const user1 = {
    id: 'user1',
    rights: [RIGHT.spareDays, RIGHT.teamsCreate],
    teams: [
      {
        id: 'team1',
        name: 'team1',
        members: [
          { person: { id: 'user1' }, role: 'manager' },
          { person: { id: 'user2' }, role: 'worker' },
        ],
      },
      {
        id: 'team3',
        name: 'team3',
        members: [{ person: { id: 'user1' }, role: 'worker' }],
      },
    ],
  };
  const user2 = {
    id: 'user2',
    roles: [ROLE.admin],
    teams: [
      {
        id: 'team1',
        name: 'team1',
        members: [
          { person: { id: 'user1' }, role: 'manager' },
          { person: { id: 'user2' }, role: 'worker' },
        ],
      },
      {
        id: 'team2',
        name: 'team2',
        members: [
          { person: { id: 'user3' }, role: 'manager' },
          { person: { id: 'user2' }, role: 'worker' },
        ],
      },
    ],
  };
  const user3 = {
    id: 'user3',
    roles: [ROLE.user],
    teams: [
      {
        id: 'team2',
        name: 'team2',
        members: [
          { person: { id: 'user3' }, role: 'manager' },
          { person: { id: 'user2' }, role: 'worker' },
        ],
      },
      {
        id: 'team3',
        name: 'team3',
        members: [{ person: { id: 'user3' }, role: 'manager' }],
      },
    ],
  };

  const user4 = {
    id: 'user4',
    roles: [ROLE.user],
    rights: [RIGHT.teamsCreate],
  };
  const loggedUser = {
    id: 'user2',
  };

  const userManager = {
    id: 'user1',

    roles: ['user'],

    teams: [
      {
        id: '5f1a22aa4001aa52c2bb2bb1',

        members: [
          {
            person: {
              id: 'user2',
            },
            role: 'worker',
          },
          {
            person: {
              id: 'user1',
            },
            role: 'manager',
          },
        ],
      },
    ],
  };

  const userHeadWorker = {
    id: 'user1',

    roles: ['user'],

    teams: [
      {
        id: '5f1a22aa4001aa52c2bb2bb1',

        members: [
          {
            person: {
              id: 'user2',
            },
            role: 'worker',
          },
          {
            person: {
              id: 'user1',
            },
            role: 'headWorker',
          },
        ],
      },
    ],
  };

  const userWorker = {
    id: 'user2',

    roles: ['user'],

    teams: [
      {
        id: 'team',

        members: [
          {
            person: {
              id: 'userManager',
            },
            role: 'manager',
          },

          {
            person: {
              id: 'user2',
            },
            role: 'worker',
          },
        ],
      },
    ],
  };

  const team = {
    id: 'teamId',
    name: 'team',
    members: [
      {
        person: {
          id: 'user1',
        },
        role: 'manager',
      },

      {
        person: {
          id: 'user2',
        },
        role: 'worker',
      },
    ],
  };

  it('should check if user has spareDays right', () => {
    expect(hasSpareDaysRight(user1)).toBeTruthy();
    expect(hasSpareDaysRight(user2)).toBeTruthy();
    expect(hasSpareDaysRight(user3)).toBeFalsy();
  });
  it('should check if user is admin', () => {
    expect(isAdmin(user1)).toBeFalsy();
    expect(isAdmin(user2)).toBeTruthy();
    expect(isAdmin(user3)).toBeFalsy();
  });
  it('should check if user is loggedUser', () => {
    expect(isLoggedUser(user1, loggedUser)).toBeFalsy();
    expect(isLoggedUser(user2, loggedUser)).toBeTruthy();
    expect(isLoggedUser(user3, loggedUser)).toBeFalsy();
  });
  it('should check if user is admin or loggedUser', () => {
    expect(isAdminOrLoggedUser(user1, loggedUser)).toBeFalsy();
    expect(isAdminOrLoggedUser(user2, loggedUser)).toBeTruthy();
    expect(isAdminOrLoggedUser(user3, loggedUser)).toBeFalsy();
  });
  it('should check if user has teamsCreate right', () => {
    expect(hasTeamsCreateRightOrManager(user1)).toBeTruthy();
    expect(hasTeamsCreateRightOrManager(user2)).toBeTruthy();
    expect(hasTeamsCreateRightOrManager(user3)).toBeTruthy();
    expect(hasTeamsCreateRightOrManager(user4)).toBeTruthy();
  });
  it('should check if user is admin or manager/headWorker in a team', () => {
    expect(isAdminOrManagerInTeam(userManager)).toBeTruthy();
    expect(isAdminOrManagerInTeam(userHeadWorker)).toBeTruthy();
    expect(isAdminOrManagerInTeam(userWorker)).toBeFalsy();
    expect(isAdminOrManagerInTeam(user2)).toBeTruthy();
  });
  it('should check if first user isManager of the second user', () => {
    expect(isManagerOfUser(user1, user2)).toBeTruthy();
    expect(isManagerOfUser(user1, user3)).toBeFalsy();
    expect(isManagerOfUser(user2, user3)).toBeFalsy();
    expect(isManagerOfUser(user3, user1)).toBeFalsy();
  });

  it('should check users work toghether', () => {
    expect(workTogether(user1, user2)).toBeTruthy();
    expect(workTogether(user2, user1)).toBeTruthy();
    expect(workTogether(user2, user3)).toBeTruthy();
    expect(workTogether(user1, user3)).toBeFalsy();
    expect(workTogether(loggedUser, user4)).toBeFalsy();
  });
  it('should check users is member in team', () => {
    expect(isMember(team, user1)).toBeTruthy();
    expect(isMember(team, user2)).toBeTruthy();
    expect(isMember(team, user3)).toBeFalsy();
  });
});

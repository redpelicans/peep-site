import { makeVar } from '@apollo/client';

export const loggedUserIdVar = makeVar();
export const calendarsCurrentMonthVar = makeVar({});

import React from 'react';
import PropTypes from 'prop-types';
import { KeyboardDatePicker } from '@material-ui/pickers';

const DateField = ({ input: { onChange, value }, ...rest }) => {
  return (
    <KeyboardDatePicker
      {...rest}
      disableToolbar
      variant="inline"
      format="MM/dd/yyyy"
      margin="normal"
      id="date-picker-inline"
      label="Date picker inline"
      value={value}
      onChange={onChange}
      KeyboardButtonProps={{
        'aria-label': 'change date',
      }}
    />
  );
};

DateField.propTypes = {
  input: PropTypes.object,
};

export default DateField;

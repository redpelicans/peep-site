import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { map } from 'ramda';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Chip from '@material-ui/core/Chip';
import { UserAvatar } from '../../components/Avatar';

const useStyles = makeStyles(theme => ({
  formControl: {
    width: '100%',
  },
  chip: {
    margin: '2px',
  },

  inputRoot: {
    padding: '6px 24px 7px 0',
  },
  input: {
    minWidth: 0,
  },
  popupIndicator: {
    padding: 2,
    color: theme.palette.action.active,
    position: 'absolute',
    right: 0,
  },
  clearIndicator: {
    position: 'absolute',
    right: 25,
    padding: 2,
    color: theme.palette.action.active,
    visibility: 'hidden',
  },
}));

const MultipleSelect = ({ input: { value, onChange }, suggestion, label, width }) => {
  const classes = useStyles();

  return (
    <Autocomplete
      multiple
      options={suggestion}
      renderOption={option => (
        <Chip key={option.id} avatar={<UserAvatar user={option} />} label={option.fullname} color="primary" />
      )}
      onChange={(event, value) => onChange(value)}
      value={value}
      renderTags={selections =>
        map(
          selected => (
            <Chip
              key={selected.value}
              avatar={<Avatar src={selected.label.picture} />}
              label={selected.fullname}
              color="primary"
              className={classes.chip}
            />
          ),
          selections,
        )
      }
      renderInput={params => <TextField label={label} fullWidth {...params} />}
      className={classNames(width, classes.formControl)}
      classes={{
        popupIndicator: classes.popupIndicator,
        inputRoot: classes.inputRoot,
        input: classes.input,
        clearIndicator: classes.clearIndicator,
      }}
    />
  );
};

MultipleSelect.propTypes = {
  input: PropTypes.object.isRequired,
  suggestion: PropTypes.array,
  label: PropTypes.object,
  width: PropTypes.string,
};

export default MultipleSelect;

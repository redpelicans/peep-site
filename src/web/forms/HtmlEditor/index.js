import React from 'react';
import PropTypes from 'prop-types';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const HtmlEditor = ({ input: { value, onChange, ...restInput }, ...rest }) => {
  return (
    <Editor
      {...rest}
      editorState={value}
      onEditorStateChange={onChange}
      toolbar={{
        options: ['inline', 'blockType', 'list', 'emoji'],
        inline: {
          options: ['bold', 'italic', 'underline', 'strikethrough'],
        },
        blockType: {
          inDropdown: true,
          options: ['Normal', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6'],
        },
        list: {
          options: ['unordered', 'ordered', 'indent', 'outdent'],
        },
      }}
      inputProps={restInput}
    />
  );
};
HtmlEditor.propTypes = {
  input: PropTypes.object,
};
export default HtmlEditor;

import React from 'react';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { FormHelperText } from '@material-ui/core';

const SelectField = ({ input: { value, name, onChange, ...restInput }, required, label, meta, ...rest }) => {
  const showError = meta.touched && required ? !meta.valid : meta.error && meta.touched && meta.dirty;
  return (
    <FormControl required={required} fullWidth>
      <InputLabel>{label}</InputLabel>
      <Select
        {...rest}
        name={name}
        inputProps={restInput}
        onChange={onChange}
        value={value}
        helpertext={showError ? meta.error || meta.submitError : undefined}
        error={showError}
      />
      {showError && <FormHelperText style={{ color: 'red' }}>{meta.error || meta.submitError}</FormHelperText>}
    </FormControl>
  );
};

SelectField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.object,
  meta: PropTypes.object,
  required: PropTypes.bool,
};

export default SelectField;

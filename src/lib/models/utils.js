import { format } from 'date-fns';
import { is } from 'ramda';

const DATE_FORMAT = 'MMddyy';
export const mdy = date => format(is(String, date) ? new Date(date) : date, DATE_FORMAT);

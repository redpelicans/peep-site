import { reduce, sum, map, prop, values, compose } from 'ramda';
import addDays from 'date-fns/addDays'
import startOfDay from 'date-fns/startOfDay'
import getHours from 'date-fns/getHours'
import isWeekend from 'date-fns/isWeekend'
import { mdy } from './utils'

const isAM = date => getHours(date) < 13;
const isPM = date => getHours(date) >= 11;
const isSameDay = (d1, d2) => mdy(d1) === mdy(d2);

class Days{
  constructor({ event, from, to, spareDays}, {keepSpareDays}={}){
    this.from = from || event?.from;
    this.to = to || event?.to;
    this.elems = {};

    if(!keepSpareDays){
      this.spareDays = spareDays ? reduce(
        (acc, day) => {
          acc[mdy(day.from)] = day;
          return acc;
        },
        {},
        spareDays,
      ) : {};
    } else this.spareDays = spareDays;

    if(event && this.to >= event.from && this.from <= event.to){
      this.start = this.from ? event.from <= this.from ?  this.from : event.from : event.from;
      this.end = this.to ? event.to >= this.to ? this.to : event.to: event.to;
      this.isSameDay = isSameDay(this.start, this.end);
      this._build();
    }
  }


  append(days){
    reduce(
      (acc, elem) => {
        if(!acc[elem.mdy]) acc[elem.mdy] = elem;
        else if(acc[elem.mdy].value < elem.value) acc[elem.mdy] = elem;
        else if(acc[elem.mdy].value + elem.value === 1 && acc[elem.mdy].type !== elem.type) acc[elem.mdy] = new Day(startOfDay(elem.date));
        return acc;
      },
      this.elems,
      values(days.elems),
    );
    return this;
  }

  get count(){
    return compose(
      sum,
      map(prop('value')),
      values,
    )(this.elems);
  }

  _isSpareDay(date){
    return this.spareDays[mdy(date)];
  }

  _build(){
    const [firstElem, lastElem] = this._getBounds();
    let currentElem = firstElem;
    if(this.isSameDay){
      this._submit(firstElem);
      return;
    }

    do {
      this._submit(currentElem);
      currentElem = currentElem.next();
    } while(!isSameDay(currentElem.date, lastElem.date))

    if(!this.isSameDay) this._submit(lastElem);
  }

  _submit(elem){
    if(!isWeekend(elem.date) && !this._isSpareDay(elem.date)) this.elems[elem.mdy] = elem;
  }

  _getBounds(){
    if(this.isSameDay){
      const bound = this._getFirstBound();
      return [bound, bound];
    }
    return [this._getFirstBound(), this._getLastBound()];
  }

  _getFirstBound(){
    if(isPM(this.start)) return new PM(this.start);
    if(this.isSameDay && isAM(this.end)) return new AM(this.start);
    return new Day(this.start);
  }

  _getLastBound(){
    if(isAM(this.end)) return new AM(this.end);
    return new Day(this.end);
  }

}

class Elem{
  constructor(date){
    this.date = date;
    this.mdy = mdy(date);
  }
}

class Day extends Elem{
  constructor(date){
    super(date);
    this.type = 'day';
    this.value = 1;
  }

  next(){
    return new Day(startOfDay(addDays(this.date, 1)))
  }
}

class PM extends Elem{
  constructor(date){
    super(date);
    this.type = 'pm';
    this.value = 0.5;
  }

  next(){
    return new Day(startOfDay(addDays(this.date, 1)))
  }
}

class AM extends Elem{
  constructor(date){
    super(date);
    this.type = 'am';
    this.value = 0.5;
  }
}

export const event2Days = (event, { spareDays, from, to }={}) => new Days({event, from, to, spareDays});

export const events2Days = (events=[], { spareDays, from, to }={}) => reduce(
  (acc, event) => acc.append(new Days({event, from, to, spareDays: acc.spareDays}, { keepSpareDays: true })),
  new Days({ spareDays, from, to }),
  events,
);



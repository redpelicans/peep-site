import startOfDay from 'date-fns/startOfDay'
import endOfDay from 'date-fns/endOfDay'
import { events2Days, event2Days } from '../days';

describe('lib | models | interval', () => {
  const event1 = {
    from: startOfDay(new Date(2020, 11, 4)),
    to: new Date(2020, 11, 22, 11, 59),
  };

  const event2 = {
    from: startOfDay(new Date(2020, 11, 22)),
    to: new Date(2020, 11, 22, 11, 59),
  };

  const event3 = {
    from: new Date(2020, 11, 22, 11, 59),
    to: endOfDay(new Date(2020, 11, 22)),
  };

  const event4 = {
    from: new Date(2020, 11, 22, 11, 59),
    to: new Date(2020, 11, 23, 11, 59),
  };

  const event5 = {
    from: startOfDay(new Date(2020, 11, 25)),
    to: endOfDay(new Date(2020, 11, 30)),
  };


  it('should get working days from event1', () => {
    expect(event2Days(event1).count).toEqual(12.5);
  });

  it('should get working days from event2', () => {
    expect(event2Days(event2).count).toEqual(.5);
  });

  it('should get working days from event3', () => {
    expect(event2Days(event3).count).toEqual(.5);
  });

  it('should get working days from event4', () => {
    expect(event2Days(event4).count).toEqual(1);
  });

  it('should get working days from event1 with spare days', () => {
    const spareDays = [
      { from: startOfDay(new Date(2020, 11, 7)) },
      { from: startOfDay(new Date(2020, 11, 22)) },
      { from: startOfDay(new Date(2020, 11, 4)) },
      { from: startOfDay(new Date(2020, 11, 29)) },
    ];
    expect(event2Days(event1, { spareDays }).count).toEqual(10);
  });

  it('should get working days from events', () => {
    const spareDays = [
      { from: startOfDay(new Date(2020, 11, 7)) },
      { from: startOfDay(new Date(2020, 11, 4)) },
      { from: startOfDay(new Date(2020, 11, 29)) },
    ];
    expect(events2Days([event1, event4, event5], { spareDays }).count).toEqual(14.5);
  });

  it('should get working days from event for a period', () => {
    const spareDays = [ { from: startOfDay(new Date(2020, 11, 7)) } ];
    expect(event2Days(event1, { spareDays, from: startOfDay(new Date(2020, 11, 7)), to: endOfDay(new Date(2020, 11, 7)) }).count).toEqual(0);
  });

  it('should get working days from events for a period', () => {
    const spareDays = [ { from: startOfDay(new Date(2020, 11, 7)) } ];
    expect(events2Days([event1, event4, event5], { spareDays, from: startOfDay(new Date(2020, 11, 6)), to: endOfDay(new Date(2020, 11, 8)) }).count).toEqual(1);
    expect(events2Days([event1, event4, event5], { from: startOfDay(new Date(2023, 11, 6)), to: endOfDay(new Date(2023, 11, 8)) }).count).toEqual(0);
    expect(events2Days([event1, event4, event5], { from: startOfDay(new Date(1999, 11, 6)), to: endOfDay(new Date(1999, 11, 8)) }).count).toEqual(0);
  });


  it('should get zero working days', () => {
    expect(events2Days().count).toEqual(0);
  });


});

import { getActiveWorkers, isCompany, isProject } from '../teams';
import { ROLE } from '../members';

describe('lib | models | teams', () => {
  it('should get isCompany', () => {
    const team1 = {
      type: 'company',
    };
    const team2 = {
      type: 'project',
    };
    const team3 = undefined;
    expect(isCompany(team1)).toBeTruthy();
    expect(isCompany(team2)).toBeFalsy();
    expect(isCompany(team3)).toBeFalsy();
  });

  it('should get isProject', () => {
    const team1 = {
      type: 'project',
    };
    const team2 = {
      type: 'company',
    };
    const team3 = undefined;
    expect(isProject(team1)).toBeTruthy();
    expect(isProject(team2)).toBeFalsy();
    expect(isProject(team3)).toBeFalsy();
  });

  describe('lib | models | teams | Active members', () => {
    const team1 = { members: [{ role: ROLE.worker, personId: '1' }] };
    const team2 = { members: [{ activityPeriods: [], role: ROLE.worker, personId: '1' }] };
    const team3 = {
      members: [
        {
          role: ROLE.worker,
          personId: '1',
          activityPeriods: [
            {
              expirationDate: new Date(2011, 0, 1),
            },
            {
              inceptionDate: new Date(2020, 0, 1),
              expirationDate: new Date(2020, 11, 32),
            },
            {
              inceptionDate: new Date(2030, 0, 1),
            },
          ],
        },
      ],
    };

    it('should get activeWorkers', () => {
      expect(getActiveWorkers(team1, new Date(), new Date())[0].personId).toEqual('1');
      expect(getActiveWorkers(team2, new Date(), new Date())[0].personId).toEqual('1');
      expect(getActiveWorkers(team3, new Date(2020, 4, 1), new Date(2022, 0, 1))[0].personId).toEqual('1');
      expect(getActiveWorkers(team3, new Date(2020, 4, 1), new Date(2022, 0, 1))[0].personId).toEqual('1');
      expect(getActiveWorkers(team3, new Date(2020, 4, 1), new Date(2020, 5, 1))[0].personId).toEqual('1');
      expect(getActiveWorkers(team3, new Date(2019, 4, 1), new Date(2021, 5, 1))[0].personId).toEqual('1');
    });

    it('should not get activeWorkers', () => {
      expect(getActiveWorkers(team3, new Date(2022, 4, 1), new Date(2019, 0, 1)).length).toEqual(0);
      expect(getActiveWorkers(team3, new Date(2022, 5, 1), new Date(2019, 0, 1)).length).toEqual(0);
    });
  });
});

import { isActiveMember, isWorker, isHeadWorker, isObserver, isManager } from '../members';

describe('lib | models | members', () => {
  it('should get isWorker', () => {
    const member1 = {
      role: 'worker',
    };
    const member2 = {
      role: 'observer',
    };
    const member3 = undefined;
    expect(isWorker(member1)).toBeTruthy();
    expect(isWorker(member2)).toBeFalsy();
    expect(isWorker(member3)).toBeFalsy();
  });

  it('should get isHeadWorker', () => {
    const member1 = {
      role: 'headWorker',
    };
    const member2 = {
      role: 'observer',
    };
    const member3 = undefined;
    expect(isHeadWorker(member1)).toBeTruthy();
    expect(isHeadWorker(member2)).toBeFalsy();
    expect(isHeadWorker(member3)).toBeFalsy();
  });

  it('should get isManager', () => {
    const member1 = {
      role: 'manager',
    };
    const member2 = {
      role: 'observer',
    };
    const member3 = undefined;
    expect(isManager(member1)).toBeTruthy();
    expect(isManager(member2)).toBeFalsy();
    expect(isManager(member3)).toBeFalsy();
  });

  it('should get isObserver', () => {
    const member1 = {
      role: 'observer',
    };
    const member2 = {
      role: 'manager',
    };
    const member3 = undefined;
    expect(isObserver(member1)).toBeTruthy();
    expect(isObserver(member2)).toBeFalsy();
    expect(isObserver(member3)).toBeFalsy();
  });

  it('should get active member', () => {
    const member1 = {
      activityPeriods: [
        {
          inceptionDate: new Date(2000, 0, 1),
          expirationDate: new Date(2010, 0, 1),
        },
        {
          inceptionDate: new Date(2015, 0, 1),
        },
      ],
    };
    const member2 = {
      activityPeriods: [
        {
          expirationDate: new Date(2010, 0, 1),
        },
      ],
    };
    const member3 = {};

    expect(isActiveMember(member1, new Date(1999, 0, 1), new Date(1999, 11, 1))).toBeFalsy();
    expect(isActiveMember(member1, new Date(2000, 0, 1), new Date(2000, 1, 1))).toBeTruthy();
    expect(isActiveMember(member1, new Date(2010, 0, 1), new Date(2016, 1, 1))).toBeTruthy();
    expect(isActiveMember(member2, new Date(2009, 0, 1), new Date(2016, 0, 1))).toBeTruthy();
    expect(isActiveMember(member2, new Date(2011, 0, 1), new Date(2016, 0, 1))).toBeFalsy();
    expect(isActiveMember(member3, new Date(), new Date())).toBeTruthy();
  });
});

import { isVacation, isSickLeave, isUnpaidLeave, isDayOff } from '../events';

describe('lib | models | teams', () => {
  const event1 = {
    type: 'vacation',
  };
  const event2 = {
    type: 'sickLeave',
  };
  const event3 = {
    type: 'unpaidLeave',
  };
  const event4 = {
    type: 'dayOff',
  };
  it('should check if event is vacation', () => {
    expect(isVacation(event1)).toBeTruthy();
    expect(isVacation(event2)).toBeFalsy();
  });
  it('should check if event is sickLeave', () => {
    expect(isSickLeave(event2)).toBeTruthy();
    expect(isSickLeave(event3)).toBeFalsy();
  });
  it('should check if event is unpaidLeave', () => {
    expect(isUnpaidLeave(event3)).toBeTruthy();
    expect(isUnpaidLeave(event4)).toBeFalsy();
  });
  it('should check if event is dayOff', () => {
    expect(isDayOff(event4)).toBeTruthy();
    expect(isDayOff(event1)).toBeFalsy();
  });
});

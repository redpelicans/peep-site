const proxy = require('http-proxy-middleware');
const { proxy: target } = require('../package.json');

module.exports = app => {
  app.use(
    ['/api', '/graphql', '/healthcheck'],
    proxy({
      target,
      changeOrigin: true,
    }),
  );
};

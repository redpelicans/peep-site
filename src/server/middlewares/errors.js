const error = (err, req, res, next) => {
  if (!err) return next();
  if (err.stack && process.env.NODE_ENV !== 'test') console.error(err.stack); // eslint-disable-line no-console
  // console.error(err.stack); // eslint-disable-line no-console
  const code = err.code || 500;
  if (err.data) return res.status(code).json(err.data);
  return res.sendStatus(code);
};

module.exports = error;

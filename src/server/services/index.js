import initHealthcheck from './healthcheck';
import initApi from './api';
import { initResources } from '../utils';

const resources = [initHealthcheck, initApi];
export default initResources(resources);

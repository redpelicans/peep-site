import { graphql } from 'graphql';
import { HTTPError } from '../../utils/errors';

const NAME = 'healthcheck';
async function doHealthcheck() {
  const {
    startTime,
    config: { gitHash },
    apollo,
    models,
  } = this.globals();

  const res = {
    startTime,
    gitHash,
  };

  try {
    res.graphql = await graphql(
      apollo.schema,
      `
        {
          _allUsersMeta {
            count
          }
        }
      `,
      null,
      { models, ctx: this.globals },
    );
    return res;
  } catch (err) {
    res.graphql = {
      ...res.graphql,
      status: 'error',
      message: err.toString(),
    };
    throw new HTTPError(500, err.toString(), res);
  }
}

const healthcheck = {
  get: doHealthcheck,
};

export default evtx => evtx.use(NAME, healthcheck);

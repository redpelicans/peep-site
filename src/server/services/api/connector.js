import { map, mergeAll } from 'ramda';
import HTTPStatus from 'http-status';
import { UnknownServiceError, UnknownMethodError } from 'evtx';
import { HTTPError } from '../../utils/errors';

export const parseUrl = url => {
  const re = new RegExp(/^\/+(\w+)\/+(\w+)\/*(\w*)/);
  const [_, service, method, id] = re.exec(url) || []; // eslint-disable-line no-unused-vars
  return map(x => x || undefined, [service, method, id]);
};

const getInput = (id, req) => mergeAll([req.query, req.body, id && { id }]);
const getMessage = req => {
  const [service, method, id] = parseUrl(req.path);
  const input = getInput(id, req);
  return { service, method, input };
};

export default evtx => (req, res, next) => {
  evtx
    .run(getMessage(req), { req, res })
    .then(result => {
      if (!result) return;
      return res.status(HTTPStatus.OK).json(result);
    })
    .catch(err => {
      if (err instanceof UnknownMethodError || err instanceof UnknownServiceError)
        return next(new HTTPError(404, 'service not found'));
      return next(err);
    });
};

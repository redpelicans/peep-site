import { contains, assocPath, path, prop } from 'ramda';
import njwt from 'njwt';
import bcrypt from 'bcryptjs';
import axios from 'axios';
import { HTTPError } from '../../utils/errors';
import debug from '../../debug';

export const salt = bcrypt.genSaltSync(10);
export const cryptPassword = password => bcrypt.hashSync(password, salt);

export const getUser = async ctx => {
  const {
    models,
    config: { secretKey },
  } = ctx.globals();
  const { req } = ctx.locals || {};

  const token = path(['headers', 'authorization'], req);
  if (!token) return Promise.resolve(ctx);

  const user = await models.people.loadFromToken(token, secretKey);
  return assocPath(['locals', 'user'], user)(ctx);
};

export const checkUser = role => ctx => {
  const { user } = ctx.locals;
  if (!user) return Promise.reject(new HTTPError(403));
  if (role && !contains(role, user.roles || [])) return Promise.reject(new HTTPError(403));
  return ctx;
};

export const getToken = ({ _id }, secretKey, expirationDate) => {
  const claims = {
    sub: _id,
    iss: 'https://drsa-admin.com',
  };
  const jwt = njwt.create(claims, secretKey);
  jwt.setExpiration(expirationDate);
  return jwt.compact();
};

export const noOutput = () => ctx => Promise.resolve({ ...ctx, output: {} });

export const emitEvent = name => ctx => {
  debug.info(name);
  ctx.emit(name, ctx);
  return Promise.resolve(ctx);
};

const TOKENINFO = 'https://graph.facebook.com/v7.0/debug_token';

export const checkFacebookUser = ({ accessToken: token, clientId, appId, secret }) =>
  axios
    .get(`${TOKENINFO}?input_token=${token}&access_token=${appId}|${secret}`)
    .then(prop('data'))
    .then(({ data }) => {
      if (data.user_id !== clientId || data.app_id !== appId) throw new Error('Cannot check Facebook token!');
      return data;
    });

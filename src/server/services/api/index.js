import debug from '../../debug';
import evtX from 'evtx';
import initAuth from './auth';
import { initServices } from '../utils';
import { getUser } from './utils';

const services = [initAuth];

export default ctx => {
  const api = evtX(ctx).configure(initServices(services)).before(getUser);
  debug.info('Api service up.');
  return ctx({ services: { ...ctx().services, api } });
};

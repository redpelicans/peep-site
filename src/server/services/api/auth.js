import { omit } from 'ramda';
import axios from 'axios';
import bcrypt from 'bcryptjs';
import { addHours } from 'date-fns';
import { HTTPError } from '../../utils/errors';
import { isPending, isInActive, isActive, STATUS } from '../../models/statuses';
import { ROLE } from '../../models/roles';
import { emitEvent, noOutput, getToken, checkUser, checkFacebookUser } from './utils';
import debug from '../../debug';
import { makeSiteUrl } from '../../utils';

const NAME = 'auth';
const TOKENINFO = 'https://www.googleapis.com/oauth2/v3/tokeninfo';

const checkGoogleUser = async (token, clientId) =>
  axios(`${TOKENINFO}?id_token=${token}`).then(({ data }) => {
    if (data.aud !== clientId) throw new Error('Wrong Google clientId!');
    return data;
  });

const service = {
  async signUp({ firstname, lastname, email, password }) {
    const { models } = this.globals();

    const user = {
      firstname,
      lastname,
      email,
      password,
      status: STATUS.pending,
      roles: [ROLE.user],
    };

    const person = await models.people.create(user);
    return { email: person.email };
  },

  async signIn({ email, password }) {
    const {
      models,
      config: { secretKey, sessionDuration },
    } = this.globals();
    const person = await models.people.loadByEmail(email);
    if (!person || !isActive(person)) return Promise.reject(new HTTPError(401));
    const isPasswordEqual = bcrypt.compareSync(password, person.password);
    if (!isPasswordEqual) return Promise.reject(new HTTPError(401));
    const expires = addHours(new Date(), sessionDuration);
    const token = getToken(person, secretKey, expires);
    debug.info(`User ${email} signed in.`);
    return { user: omit(['password'], person), token };
  },

  signInWithGoogle({ token }) {
    const {
      models,
      config: {
        secretKey,
        sessionDuration,
        google: { clientId },
      },
    } = this.globals();
    return checkGoogleUser(token, clientId)
      .then(async ({ email }) => {
        const person = await models.people.loadByEmail(email);
        if (!person || !isActive(person)) return Promise.reject(new HTTPError(401));
        const expires = addHours(new Date(), sessionDuration);
        const token = getToken(person, secretKey, expires);
        debug.info(`User ${email} signed in with google`);
        return { user: omit(['password'], person), token };
      })
      .catch(err => {
        debug.error(err);
        throw new HTTPError(401);
      });
  },
  async signUpWithGoogle({ token }) {
    const {
      models,
      config: {
        google: { clientId },
      },
    } = this.globals();
    return checkGoogleUser(token, clientId)
      .then(async ({ email, given_name, family_name }) => {
        const user = {
          firstname: given_name,
          lastname: family_name,
          email,
          status: STATUS.pending,
          roles: [ROLE.user],
        };

        const person = await models.people.create(user);
        return { email: person.email };
      })
      .catch(err => {
        debug.error(err);
        throw new HTTPError(401);
      });
  },

  signInWithFacebook({ accessToken, userID, email }) {
    const {
      models,
      config: { facebook, secretKey, sessionDuration },
    } = this.globals();
    return checkFacebookUser({ ...facebook, accessToken, clientId: userID }).then(async () => {
      const person = await models.people.loadByEmail(email);
      if (!person || !isActive(person)) return Promise.reject(new HTTPError(401));
      const expires = addHours(new Date(), sessionDuration);
      const token = getToken(person, secretKey, expires);
      debug.info(`User ${email} signed in with facebook`);
      return { user: omit(['password'], person), token };
    });
  },

  signUpWithFacebook({ accessToken, first_name, last_name, email }) {
    const {
      models,
      config: { facebook },
    } = this.globals();
    return checkFacebookUser({ ...facebook, accessToken }).then(async () => {
      const user = {
        firstname: first_name,
        lastname: last_name,
        email,
        status: STATUS.pending,
        roles: [ROLE.user],
      };

      const person = await models.people.create(user);
      return { email: person.email };
    });
  },

  getUser() {
    const { user } = this.locals;
    return Promise.resolve(omit(['password'], user));
  },

  signOut() {
    const { user } = this.locals;
    debug.info(`User ${user.email} signed out.`);
    return Promise.resolve({ _id: user._id, email: user.email });
  },

  async requestNewPassword({ email }) {
    return { email };
  },

  async invite({ id }) {
    return { id };
  },

  async confirmSignUpAccount({ id }) {
    const {
      models,
      config: { secretKey },
    } = this.globals();
    const { res } = this.locals;

    const person = await models.people.loadFromToken(`Bearer ${id}`, secretKey);
    if (!person) return Promise.reject(new HTTPError(401));
    if (isPending(person)) await models.people.updateOne({ _id: person._id, status: STATUS.active });
    res.redirect(makeSiteUrl(this.globals(), '/'));
    return { user: omit(['password'], person) };
  },

  async confirmAccount({ password }) {
    const { user } = this.locals;
    const {
      config: { secretKey, sessionDuration },
      models,
    } = this.globals();

    if (!isPending(user) && !isInActive(user)) return Promise.reject(new HTTPError(400));
    const version = await models(user, { authRequired: true }).people.updateOne({
      ...user,
      password,
      status: STATUS.active,
    });
    const expires = addHours(new Date(), sessionDuration);
    const token = getToken(version, secretKey, expires);
    return { user: omit(['password'], version), token };
  },

  async confirmAccountWithGoogle({ token: googleToken }) {
    const { user } = this.locals;
    const {
      config: {
        secretKey,
        sessionDuration,
        google: { clientId },
      },
      models,
    } = this.globals();

    if (!isPending(user) && !isInActive(user)) return Promise.reject(new HTTPError(400));
    return checkGoogleUser(googleToken, clientId)
      .then(async ({ email }) => {
        if (email !== user.email) throw new HTTPError(401, 'Wrong email address');
        const version = await models(user, { authRequired: true }).people.updateOne({
          ...user,
          status: STATUS.active,
        });
        const expires = addHours(new Date(), sessionDuration);
        const token = getToken(version, secretKey, expires);
        return { user: omit(['password'], version), token };
      })
      .catch(err => {
        debug.error(err);
        throw new HTTPError(401);
      });
  },

  async confirmAccountWithFacebook({ accessToken, userID, email }) {
    const { user } = this.locals;
    const {
      models,
      config: { facebook, secretKey, sessionDuration },
    } = this.globals();

    if (!isPending(user) && !isInActive(user)) return Promise.reject(new HTTPError(400));
    return checkFacebookUser({ ...facebook, accessToken, clientId: userID })
      .then(async () => {
        if (email !== user.email) throw new HTTPError(401, 'Wrong email address');
        const version = await models(user, { authRequired: true }).people.updateOne({
          ...user,
          status: STATUS.active,
        });
        const expires = addHours(new Date(), sessionDuration);
        const token = getToken(version, secretKey, expires);
        return { user: omit(['password'], version), token };
      })
      .catch(err => {
        debug.error(err);
        throw new HTTPError(401);
      });
  },

  async resetPassword({ password }) {
    const { user } = this.locals;
    const {
      config: { secretKey, sessionDuration },
      models,
    } = this.globals();

    if (!isActive(user)) return Promise.reject(new HTTPError(400));
    const version = await models(user, { authRequired: true }).people.updateOne({ ...user, password });
    const expires = addHours(new Date(), sessionDuration);
    const token = getToken(version, secretKey, expires);
    return { user: omit(['password'], version), token };
  },
};

export default evtx =>
  evtx
    .use(NAME, service)
    .service(NAME)
    .before({
      signOut: [checkUser()],
      getUser: [checkUser()],
      confirmAccount: [checkUser()],
      resetPassword: [checkUser()],
    })
    .after({
      requestNewPassword: [emitEvent('person:requestNewPassword')],
      invite: [checkUser(ROLE.admin), emitEvent('person:invited')],
      register: [emitEvent('person:registered'), noOutput()],
      confirmAccount: [emitEvent('person:confirmed')],
      confirmAccountWithGoogle: [emitEvent('person:confirmed')],
      confirmAccountWithFacebook: [emitEvent('person:confirmed')],
      confirmSignUpAccount: [emitEvent('person:confirmed')],
      signIn: [emitEvent('person:signedIn')],
      signUp: [emitEvent('person:signedUp')],
      signInWithGoogle: [emitEvent('person:signedIn')],
      signInWithFacebook: [emitEvent('person:signedIn')],
      signUpWithGoogle: [emitEvent('person:signedUp')],
      signUpWithFacebook: [emitEvent('person:signedUp')],
    });

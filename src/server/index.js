import '@babel/register';
import '@babel/polyfill';
const debug = require('./debug');
import { initResources } from 'jeto';
import initConfig from './init/config';
import initModels from './init/models';
import initHttp from './init/http';
import initRouter from './init/router';
import initDatabase from './init/database';
import initServices from './services';
import initApollo from './init/apollo';
import initDataLoaders from './dataLoaders';
import initEmails from './init/emails';
import initReactor from './reactor';

const resources = [
  initConfig,
  initEmails,
  initDatabase,
  initModels,
  initDataLoaders,
  initServices,
  initApollo,
  initRouter,
  initHttp,
  initReactor,
];

initResources(resources)
  .then(ctx => debug.info(ctx().config))
  .then(() => debug.info('🚀 Peep server started'))
  .catch(err => debug.error(err));

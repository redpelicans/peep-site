import { addHours } from 'date-fns';
import debug from '../debug';
import { HTTPError } from '../utils/errors';
import { isPending, isActive } from '../models/statuses';
import { getToken } from '../services/api/utils';
import { makeSiteUrl } from '../utils';
import { RequestNewPasswordEmail, InvitationEmail, UserConfirmedEmail } from '../emails';
import { map } from 'ramda';

const signedUp = async ctx => {
  const { email } = ctx.output;
  const { user: author } = ctx.locals;
  const {
    email: { send },
    models,
    config: { siteUrl, secretKey, sessionDuration },
  } = ctx.globals();

  const person = await models.people.loadByEmail(email);
  if (!person) throw new HTTPError(404);

  if (!isPending(person)) throw new HTTPError(400);

  const expires = addHours(new Date(), sessionDuration * 3);
  const token = getToken(person, secretKey, expires);
  const url = makeSiteUrl(ctx.globals(), `/api/auth/confirmSignUpAccount?id=${token}`);
  const mailOptions = {
    subject: '🌴 Peep::Finalize your account',
    to: person.email,
  };
  debug.info(`Send signup confirmation email to "${person.email}"`);
  return send({
    ...mailOptions,
    html: InvitationEmail({ url, siteUrl, author, sessionDuration: sessionDuration * 3, person }),
  });
};

const invite = async ctx => {
  const { id } = ctx.output;
  const { user: author } = ctx.locals;
  const {
    email: { send },
    models,
    config: { siteUrl, secretKey, sessionDuration },
  } = ctx.globals();

  const person = await models.people.loadOne(id);
  if (!person) throw new HTTPError(404);
  if (isActive(person)) throw new HTTPError(400);
  const expires = addHours(new Date(), sessionDuration * 3);
  const token = getToken(person, secretKey, expires);
  const url = makeSiteUrl(ctx.globals(), `/confirmAccount?id=${token}`);
  const mailOptions = {
    subject: '🌴 Peep::Finalize your account',
    to: person.email,
  };
  debug.info(`Send invitation email to "${person.email}"`);
  return send({
    ...mailOptions,
    html: InvitationEmail({ url, siteUrl, author, sessionDuration: sessionDuration * 3, person }),
  });
};

const requestNewPassword = async ctx => {
  const { email } = ctx.output;
  const {
    email: { send },
    models,
    config: { secretKey, sessionDuration, siteUrl },
  } = ctx.globals();

  const person = await models.people.loadByEmail(email);
  if (!person) throw new HTTPError(404);
  if (!isActive(person)) throw new HTTPError(400);
  const expires = addHours(new Date(), sessionDuration);
  const token = getToken(person, secretKey, expires);
  const url = makeSiteUrl(ctx.globals(), `/resetPassword?id=${token}`);
  const mailOptions = {
    subject: '🌴 Peep::Reset your password',
    to: person.email,
  };
  debug.info(`Send request new password  email to "${person.email}"`);

  return send({ ...mailOptions, html: RequestNewPasswordEmail({ url, siteUrl, sessionDuration, person }) });
};

const sendUserConfirmation = async ctx => {
  const { user } = ctx.output;
  const {
    email: { send },
    models,
    config: { siteUrl },
  } = ctx.globals();
  const query = {
    roles: 'admin',
    _id: { $ne: user._id },
  };
  const admins = await models.people.collection.find(query).toArray();
  const mailOptions = person => ({
    subject: '🌴 Peep::User account confirmed',
    to: person.email,
  });
  debug.info(`User "${user.firstname} ${user.lastname} has confirmed his account"`);
  return map(admin => send({ ...mailOptions(admin), html: UserConfirmedEmail({ siteUrl, user, admin }) }))(admins);
};

const storeLastLogin = async ctx => {
  const {
    user: { _id },
  } = ctx.output;
  const { models } = ctx.globals();
  await models.people.collection.update({ _id }, { $set: { lastLogin: new Date() } });
};

export default ctx => {
  const {
    services: { api },
  } = ctx();

  api.service('auth').on('person:invited', invite);
  api.service('auth').on('person:signedUp', signedUp);
  api.service('auth').on('person:requestNewPassword', requestNewPassword);
  api.service('auth').on('person:confirmed', sendUserConfirmation);
  api.service('auth').on('person:signedIn', storeLastLogin);

  debug.info('Reactor ready to react ...');
  return ctx;
};

import express from 'express';
import debug from '../debug';
import path from 'path';
import compression from 'compression';
import bodyParser from 'body-parser';
import healthcheckConnector from '../services/healthcheck/connector';
import errorHandler from '../middlewares/errors';
import apiConnector from '../services/api/connector';

const expressPino = require('express-pino-logger')({ logger: debug });

export default async ctx => {
  const {
    apollo,
    services: { api, healthcheck },
  } = ctx();

  const app = express();
  app.use(bodyParser.json({ limit: '10mb' }));
  app.use(expressPino);
  app.use(compression());
  app.use('/healthcheck', healthcheckConnector(healthcheck));
  app.use(express.static(path.join(__dirname, '../../../build')));
  app.use('/api', apiConnector(api));
  apollo.applyMiddleware({ app });
  app.use(express.static(path.join(__dirname, '../../../build/index.html')));
  app.use((req, res) => res.sendFile(path.join(__dirname, '../../../build/index.html')));
  app.use(errorHandler);

  debug.info('Routes setup');
  return ctx({ app });
};

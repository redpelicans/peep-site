import nodemailer from 'nodemailer';
import mg from 'nodemailer-mailgun-transport';
import debug from '../debug';

const mailDefaultOptions = {
  from: 'Peep <peep@redpelicans.com>',
};

const fakeSendMail = () => Promise.resolve();

const sendMail = mgOptions => {
  if (!mgOptions) {
    debug.info(`Fake mail setup`);
    return fakeSendMail;
  }
  const transporter = nodemailer.createTransport(mg({ auth: mgOptions }));
  debug.info(`Emails transport ready`);
  return mailOptions => {
    const promise = new Promise((resolve, reject) => {
      transporter.sendMail({ ...mailDefaultOptions, ...mailOptions }, (err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
    });
    return promise;
  };
};

const init = ctx => {
  const {
    config: { mg },
  } = ctx();
  return ctx({ email: { send: sendMail(mg) } });
};

export default init;

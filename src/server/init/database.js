import debug from '../debug';
import { MongoClient } from 'mongodb';

export default ctx => {
  const {
    config: {
      mongo: { url, dbName },
    },
  } = ctx();
  const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });
  return new Promise((resolve, reject) => {
    client.connect(err => {
      if (err) return reject(err);
      debug.info('Connected successfully to mongodb server');
      resolve(ctx({ mongo: { database: client.db(dbName), close: () => client.close() } }));
    });
  });
};

import { ApolloServer } from 'apollo-server-express';
import { typeDefs, resolvers } from '../graphql/schema';
import debug from '../debug';

export default ctx => {
  const {
    dataLoaders,
    models,
    config: { secretKey },
  } = ctx();

  const getUser = async bearer => {
    if (!bearer) return;
    return await models.people.loadFromToken(bearer, secretKey);
  };

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    playground: true,
    // tracing: true,
    // logger: debug,
    formatError: err => {
      debug.error(err);
      return err;
    },
    plugins: [
      {
        serverWillStart() {
          debug.info('Apollo server started');
        },
        requestDidStart(requestContext) {
          debug.info(`❓ ${requestContext.request.query}`);
        },
      },
    ],

    context: async ({ req }) => {
      const user = await getUser(req.headers.authorization);
      return {
        user,
        ctx,
        models: models(user),
        dataLoaders,
      };
    },
  });

  return ctx({ apollo: server });
};

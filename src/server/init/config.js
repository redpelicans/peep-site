require('../env');

const PRODUCTION = 'production';

export default ctx => {
  const gitHash = process.env.GIT_HASH || 'Local';
  const isProduction = process.env.NODE_ENV === PRODUCTION;
  const httpServer = { host: process.env.SERVER_HOST || '0.0.0.0', port: Number(process.env.SERVER_PORT) || 80 };
  const mongo = {
    url: process.env.MONGODB_URL || 'mongodb://localhost:27017',
    dbName: process.env.MONGODB_DATABASE || 'peep2',
  };
  const mg = {
    api_key: process.env.MG_API_KEY,
    domain: process.env.MG_DOMAIN,
  };
  const emailSupplier = process.env.EMAIL_SUPPLIER || 'MG';
  const config = {
    secretKey: process.env.SECRET_KEY,
    sessionDuration: process.env.SESSION_DURATION || 8,
    siteUrl: process.env.SITE_URL,
    google: {
      clientId: process.env.GOOGLE_CLIENT_ID,
    },
    facebook: {
      appId: process.env.FACEBOOK_APP_ID,
      secret: process.env.FACEBOOK_SECRET,
    },
    gitHash,
    mongo,
    mg,
    isProduction,
    httpServer,
    emailSupplier,
  };

  if (process.env.LOGGING_DRIVER === 'gke') {
    const logging = {
      driver: process.env.LOGGING_DRIVER,
      projectId: process.env.LOGGING_PROJECT_ID,
      logName: process.env.LOGGING_LOGNAME,
    };
    config.logging = logging;
  }
  return ctx({ startTime: new Date(), config });
};

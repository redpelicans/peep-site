import Models from '../models';
import { ROLE } from '../models/roles';

export default async ctx => {
  const {
    mongo: { database },
  } = ctx();
  const models = await Models(database, { roles: [ROLE.api] });
  return ctx({ models });
};

import debug from '../debug';
import { getUrl } from './utils';

export default ctx => {
  const {
    app,
    config: {
      httpServer: { host, port },
    },
  } = ctx();
  return new Promise((resolve, reject) => {
    const server = app.listen(port, host, err => {
      if (err) return reject(err);
      const url = getUrl(server);
      debug.info(`Http server listen on ${url}`);
      server.url = url;
      return resolve(ctx({ app, httpServer: server }));
    });
  });
};

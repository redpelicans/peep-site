import { reduce } from 'ramda';
import bcrypt from 'bcryptjs';
import url from 'url';

export const initResources = resources => ctx =>
  reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), resources);

const salt = bcrypt.genSaltSync(10);
export const cryptPassword = password => bcrypt.hashSync(password, salt);

export const makeSiteUrl = ({ config: { siteUrl } }, to) => url.resolve(siteUrl, to);

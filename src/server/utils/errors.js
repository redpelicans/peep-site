function HTTPError(code, message, data) {
  Error.captureStackTrace(this, HTTPError);
  this.message = message || `HTTP Error code ${code}`;
  this.name = 'HTTPError';
  this.data = data;
  this.code = code;
}
HTTPError.prototype = Object.create(Error.prototype);
HTTPError.prototype.constructor = HTTPError;

function SessionTimedout() {
  Error.captureStackTrace(this, SessionTimedout);
  this.name = 'SessionTimedout';
}
SessionTimedout.prototype = Object.create(Error.prototype);
SessionTimedout.prototype.constructor = SessionTimedout;

module.exports = { SessionTimedout, HTTPError };

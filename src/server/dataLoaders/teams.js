import DataLoader from 'dataloader';
import { map, reduce, propOr, path } from 'ramda';
import { isMember, isAdmin } from '../models/roles';

export default ctx => {
  const { models } = ctx();
  return {
    loadTeam: new DataLoader(
      async teamIds => {
        const teams = await models.teams.collection.find({ _id: { $in: teamIds } }).toArray();
        const hteams = reduce(
          (acc, team) => {
            acc[team._id] = team;
            return acc;
          },
          {},
          teams,
        );
        return map(teamId => hteams[teamId], teamIds);
      },
      { cache: false },
    ),

    loadPersonTeams: new DataLoader(
      async data => {
        const personIds = map(path(['person', '_id']), data);
        const user = path([0, 'loggedUser'], data);
        const teams = await models.teams.collection.find({ 'members.personId': { $in: personIds } }).toArray();
        const hteams = reduce(
          (acc, team) => {
            for (const member of propOr([], 'members', team)) {
              if (isAdmin(user) || isMember(team, user)) {
                if (!acc[member.personId]) acc[member.personId] = [team];
                else acc[member.personId].push(team);
              }
            }
            return acc;
          },
          {},
          teams,
        );
        return map(personId => hteams[personId], personIds);
      },
      { cache: false },
    ),
  };
};

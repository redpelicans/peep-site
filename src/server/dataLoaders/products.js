import DataLoader from 'dataloader';
import { map, filter } from 'ramda';

export default ctx => {
  const { models } = ctx();
  return {
    loadItems: new DataLoader(
      async productIds => {
        const items = await models.productItems.loadAll({ productId: { $in: productIds } });
        return map(productId => filter(item => item.productId.equals(productId), items), productIds);
      },
      { cache: false },
    ),
  };
};

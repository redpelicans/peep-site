import DataLoader from 'dataloader';
import { map, reduce } from 'ramda';

export default ctx => {
  const { models } = ctx();
  return {
    loadPerson: new DataLoader(
      async personIds => {
        const people = await models.people.collection.find({ _id: { $in: personIds } }).toArray();
        const hpeople = reduce(
          (acc, person) => {
            acc[person._id] = person;
            return acc;
          },
          {},
          people,
        );
        return map(personId => hpeople[personId], personIds);
      },
      { cache: false },
    ),
  };
};

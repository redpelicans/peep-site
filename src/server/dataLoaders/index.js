import { reduce, merge } from 'ramda';
import people from './people';
import teams from './teams';

const loaders = [people, teams];

export default ctx =>
  ctx({ dataLoaders: reduce((dataLoaders, loader) => merge(loader(ctx), dataLoaders), {}, loaders) });

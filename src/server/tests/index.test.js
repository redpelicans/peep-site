import { initResources } from 'jeto';
import { startOfDay, endOfDay } from 'date-fns';
import { flatten, uniq, pluck, map, path } from 'ramda';
import got from 'got';
import { graphql } from 'graphql';
import initModels from '../init/models';
import initHttp from '../init/http';
import initRouter from '../init/router';
import initDatabase from '../init/database';
import initServices from '../services';
import initApollo from '../init/apollo';
import initDataLoaders from '../dataLoaders';
import { MEMBER_ROLE, RIGHT, ROLE } from '../models/roles';
import { STATUS } from '../models/statuses';
import { v4 as uuidv4 } from 'uuid';
import { initConfig } from './utils';

let CTX;
let runql;
let userDoc;

describe('server | graphql', () => {
  let teamId;
  let team2;
  let admin;
  let worker1;
  let worker2;
  let spareDay1;
  let event;
  let amendment;

  beforeAll(async () => {
    const resources = [
      initConfig,
      initDatabase,
      initModels,
      initDataLoaders,
      initServices,
      initApollo,
      initRouter,
      initHttp,
    ];

    try {
      await initResources(resources).then(async ctx => {
        CTX = ctx;
      });
      userDoc = {
        firstname: 'Test',
        lastname: 'Test',
        email: `${uuidv4()}@test.fr`,
        roles: [ROLE.admin],
        status: STATUS.active,
      };

      admin = await CTX().models.people.create(userDoc);
      runql = async (query, loggedUser = admin) => {
        const GCTX = {
          user: loggedUser,
          ctx: CTX,
          models: CTX().models(loggedUser, { authRequired: true }),
          dataLoaders: CTX().dataLoaders,
        };
        const res = await graphql(CTX().apollo.schema, query, null, GCTX);
        if (res.errors) throw res.errors[0];
        return res;
      };
    } catch (err) {
      console.error(err);
      CTX()
        .mongo.database.dropDatabase()
        .then(() => CTX().httpServer.close())
        .then(() => CTX().mongo.close());
    }
  });

  afterAll(() => {
    return CTX()
      .mongo.database.dropDatabase()
      .then(() => CTX().httpServer.close())
      .then(() => CTX().mongo.close());
  });

  it('should create a worker with teams:create rights', async () => {
    const query = ` mutation { createUser(firstname: "firstname", lastname: "lastname", email: "email", status: active, rights: [${RIGHT.teamsCreate}], roles: [${ROLE.user}]){ id email } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.createUser.id).toBeDefined();
    worker1 = await CTX().models.people.loadByEmail(res.createUser.email);
    expect(worker1._id.toString()).toEqual(res.createUser.id);
  });

  it('should get allUsers as admin', async () => {
    const res = await runql(`{allUsers { id  } }`).then(path(['data']));
    expect(res.allUsers.length).toEqual(2);
  });

  it('should confirm emailUniqueness', async () => {
    const res = await runql(`{checkEmailUniqueness(email: "email1234") }`).then(path(['data']));
    expect(res.checkEmailUniqueness).toBeTruthy();
  });

  it('should unconfirm emailUniqueness', async () => {
    const res = await runql(`{checkEmailUniqueness(email: "email") }`).then(path(['data']));
    expect(res.checkEmailUniqueness).toBeFalsy();
  });

  it('should create a simple worker', async () => {
    const query = ` mutation { createUser(firstname: "firstname", lastname: "lastname", email: "email2", status: active){ id email } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.createUser.id).toBeDefined();
    worker2 = await CTX().models.people.loadByEmail(res.createUser.email);
    expect(worker2._id.toString()).toEqual(res.createUser.id);
  });

  it('should ping and get 3 users', async () => {
    const res = await got(`${CTX().httpServer.url}/healthcheck`).json();
    expect(res.graphql.data._allUsersMeta.count).toEqual(3);
  });
  it('should create a team', async () => {
    const query = `mutation { 
      createTeam(
      name: "team",  
      type: project, 
      country: "FRA", 
      description: "description", 
      members: [ 
        { role: ${MEMBER_ROLE.worker} , personId: "${admin._id}"  }
      ] ){ id country members{role person{id}} } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.createTeam.id).toBeDefined();
    expect(res.createTeam.country).toEqual('FRA');
    teamId = res.createTeam.id;
    expect(res.createTeam.members[0].person.id).toEqual(admin._id.toString());
    expect(res.createTeam.members[0].role).toEqual('worker');
  });

  it('should create a team with teamsCreate right', async () => {
    const query = `mutation { 
      createTeam(
        name: "team2",  
        type: project, 
        country: "FRA", 
        description: "description", 
        members: [ 
          { role: ${MEMBER_ROLE.worker} , personId: "${worker2._id}"  }
          { role: ${MEMBER_ROLE.manager} , personId: "${worker1._id}"  }
        ] ){ id country members{role person{id}} } } `;

    const res = await runql(query, worker1).then(path(['data']));
    expect(res.createTeam.id).toBeDefined();
    team2 = res.createTeam;
  });

  it('should get allUsers as manager', async () => {
    const res = await runql(`{allUsers { id teams { id }} }`, worker1).then(path(['data']));
    expect(uniq(flatten(map(u => pluck('id', u.teams), res.allUsers))).length).toEqual(1);
    expect(map(u => u.id, res.allUsers)).toEqual([worker1._id.toString(), worker2._id.toString()]);
  });

  it('should find users as manager', async () => {
    const res = await runql(`{searchUsers(match: "email") { id firstname lastname email } }`, worker1).then(
      path(['data']),
    );
    expect(res.searchUsers.length).toEqual(2);
  });

  it('should not create a team without teamsCreate right', () => {
    const query = `mutation { createTeam(name: "team3",  type: project, country: "FRA"){ id }} `;
    expect(runql(query, worker2)).rejects.toThrow(/Unauthorized/);
  });

  it('should load users as admin', async () => {
    const res = await runql(`{allUsers{ id teams { id } }}`).then(path(['data']));
    expect(res.allUsers.length).toEqual(3);
    expect(uniq(flatten(map(u => pluck('id', u.teams), res.allUsers))).length).toEqual(2);
  });

  it('should update user', async () => {
    const query = ` mutation { updateUser(id: "${admin._id}"  ){ id status } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.updateUser.id).toEqual(admin._id.toString());
    expect(res.updateUser.status).toEqual(STATUS.active);
  });

  // it('should load User by email', async () => {
  //   const res = await runql(`{loadUserByEmail(email: "${userDoc.email}"){ id  } }`).then(
  //     path(['data']),
  //   );
  //   expect(res.loadUserByEmail.id).toBeDefined();
  // });

  // it('should load by name', async () => {
  //   const res = await runql(`{loadTeamByName(name: "team"){ id } }`).then(path(['data']));
  //   expect(res.loadTeamByName.id).toBeDefined();
  // });

  it('should unconfirm teamUniqueness', async () => {
    const res = await runql(`{checkTeamUniqueness(name: "NoTeam")}`).then(path(['data']));
    expect(res.checkTeamUniqueness).toBeTruthy();
  });

  it('should confirm teamUniqueness', async () => {
    const res = await runql(`{checkTeamUniqueness(name: "team")}`).then(path(['data']));
    expect(res.checkTeamUniqueness).toBeFalsy();
  });

  it('should unconfirm teamUniqueness with teamCreate right', async () => {
    const res = await runql(`{checkTeamUniqueness(name: "NoTeam")}`, worker1).then(path(['data']));
    expect(res.checkTeamUniqueness).toBeTruthy();
  });

  it('should confirm teamUniqueness with teamCreate right', async () => {
    const res = await runql(`{checkTeamUniqueness(name: "team")}`, worker1).then(path(['data']));
    expect(res.checkTeamUniqueness).toBeFalsy();
  });

  it('should not confirm teamUniqueness without teamCreate right', async () => {
    const query = `{checkTeamUniqueness(name: "team")}`;
    expect(runql(query, worker2)).rejects.toThrow(/Unauthorized/);
  });

  it('should get all teams', async () => {
    const query = `{ allTeams(first: 0){ id members{role person{id}} } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.allTeams[0].members[0].person.id).toEqual(admin._id.toString());
  });

  it('should create an event as an admin', async () => {
    const query = `mutation { createEvent(from: "${new Date().toISOString()}", to: "${new Date().toISOString()}", description: "description", type: vacation, teamId: "${teamId}", personId: "${
      admin._id
    }" ){ id from to person{id} team{id}} } `;
    const res = await runql(query).then(path(['data']));
    event = res.createEvent;
    expect(res.createEvent.id).toBeDefined();
    expect(res.createEvent.person.id).toEqual(admin._id.toString());
    expect(res.createEvent.team.id).toEqual(teamId);
  });

  it('should create an event as a manager', async () => {
    const query = `mutation { createEvent(from: "${new Date().toISOString()}", to: "${new Date().toISOString()}", description: "description", type: dayOff, teamId: "${
      team2.id
    }", personId: "${worker2._id}" ){ id from to person{id} team{id}} } `;
    const res = await runql(query, worker1).then(path(['data']));
    expect(res.createEvent.id).toBeDefined();
    expect(res.createEvent.person.id).toEqual(worker2._id.toString());
    expect(res.createEvent.team.id).toEqual(team2.id);
  });

  it('should not create an event', async () => {
    const query = `mutation { createEvent(from: "${new Date().toISOString()}", to: "${new Date().toISOString()}", description: "description", type: dayOff, teamId: "${teamId}", personId: "${
      worker1._id
    }" ){ id from to person{id} team{id}} } `;
    expect(runql(query, worker2)).rejects.toThrow(/Unauthorized/);
  });

  it('should get all events', async () => {
    const query = `{ allEvents(first: 0, filter: { personId: "${admin._id}", teamId: "${teamId}"}){ id } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.allEvents[0].id).toBeDefined();
  });

  it('should load all calendar events for team', async () => {
    const query = `{ allCalendarEvents(teamId: "${teamId}", from: "${new Date(
      event.from,
    ).toISOString()}", to: "${new Date(event.to).toISOString()}"){ id } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.allCalendarEvents[0].id).toBeDefined();
  });

  it('should not all calendar events for a user who does not belong to the team', async () => {
    const query = `{ allCalendarEvents(teamId: "${teamId}", from: "${new Date(
      event.from,
    ).toISOString()}", to: "${new Date(event.to).toISOString()}"){ id } } `;
    expect(runql(query, worker1)).rejects.toThrow(/Unauthorized/);
  });

  it('should filter events', async () => {
    const query = `{ allEvents(filter: { types: [vacation]}){ id } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.allEvents.length).toEqual(1);
  });

  it('should create a CountrySpareDay', async () => {
    const from = startOfDay(new Date(2020, 8, 25));
    const query = `mutation { createSpareDay(country: "FRA", from: "${from.toISOString()}", label: "label" ){ id label country } }`;
    const res = await runql(query).then(path(['data']));
    spareDay1 = res.createSpareDay;
    expect(res.createSpareDay.id).toBeDefined();
    expect(res.createSpareDay.label).toEqual('label');
    expect(res.createSpareDay.country).toEqual('FRA');
  });

  it('should create a TeamSpareDay', async () => {
    const from = startOfDay(new Date(2020, 8, 23));
    const query = `mutation { createSpareDay(teamId: "${teamId}", from: "${from.toISOString()}", label: "label" ){ id label team{ id } team { id } } }`;
    const res = await runql(query).then(path(['data']));
    expect(res.createSpareDay.id).toBeDefined();
    expect(res.createSpareDay.label).toEqual('label');
    expect(res.createSpareDay.team.id.toString()).toEqual(res.createSpareDay.team.id.toString());
  });

  it('should update a spareDay', async () => {
    let query = `{ allSpareDays(first: 0){ id } } `;
    let res = await runql(query).then(path(['data']));
    const day = res.allSpareDays[0];
    query = `mutation { updateSpareDay(id: "${day.id}", label: "label2" ){ id label country } }`;
    res = await runql(query).then(path(['data']));
    expect(res.updateSpareDay.id).toBeDefined();
    expect(res.updateSpareDay.label).toEqual('label2');
    expect(res.updateSpareDay.country).toEqual('FRA');
  });

  it('should get all spareDays', async () => {
    const query = `{ allSpareDays(first: 0){ id } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.allSpareDays.length).toEqual(2);
  });

  it('should get working days', async () => {
    const from = startOfDay(new Date(2020, 8, 21));
    const to = endOfDay(new Date(2020, 8, 27));
    const query = `{ workingDaysOfPeriod(from: "${from.toISOString()}", to: "${to.toISOString()}", teamId: "${teamId}"){ workingDays } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.workingDaysOfPeriod.workingDays).toEqual(3);
  });

  it('should not check spare day uniqueness', async () => {
    const from = startOfDay(new Date(2020, 8, 25));
    const query = `{ checkSpareDayUniqueness(country: "FRA", from: "${from.toISOString()}") }`;
    const res = await runql(query).then(path(['data']));
    expect(res.checkSpareDayUniqueness).toBeFalsy();
  });

  it('should check spare day uniqueness', async () => {
    const query = `{ checkSpareDayUniqueness(country: "XXX", from: "${new Date(1999, 1, 1).toISOString()}") }`;
    const res = await runql(query).then(path(['data']));
    expect(res.checkSpareDayUniqueness).toBeTruthy();
  });

  it('should delete team', () => {
    const query = `mutation{ deleteTeam(id: "${teamId}"){ id }}`;
    expect(runql(query)).rejects.toThrow();
  });

  it('should not delete a team not created by user', () => {
    const query = `mutation{ deleteTeam(id: "${team2.id}"){ id }}`;
    expect(runql(query, worker2)).rejects.toThrow(/Unauthorized/);
  });

  it('should delete a team created by user', async () => {
    const query = `mutation{ deleteTeam(id: "${team2.id}"){ id }}`;
    const data = await runql(query, worker1).then(path(['data']));
    expect(data.deleteTeam.id).toEqual(team2.id);
  });

  it('should not delete a spareDay without spareDays right', () => {
    const query = `mutation { deleteSpareDay(id: "${spareDay1.id}"){ id } }`;
    expect(runql(query, worker1)).rejects.toThrow(/Unauthorized/);
  });

  it('should create an amendment', async () => {
    const query = `mutation { createAmendment(from: "${new Date().toISOString()}", to: "${new Date().toISOString()}", description: "description", teamId: "${teamId}", personId: "${
      admin._id
    }", days: 5 ){ id from to person{id} team{id}} } `;
    const res = await runql(query).then(path(['data']));
    amendment = res.createAmendment;
    expect(res.createAmendment.id).toBeDefined();
    expect(res.createAmendment.person.id).toEqual(admin._id.toString());
    expect(res.createAmendment.team.id).toEqual(teamId);
  });

  it('should get all amendments', async () => {
    const query = `{ allAmendments(first:0){ id } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.allAmendments[0].id).toBeDefined();
  });
  it('should update amendment', async () => {
    const query = ` mutation { updateAmendment(id: "${amendment.id}",teamId:"${teamId}" ,days : 3 ){ id days } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.updateAmendment.id).toEqual(amendment.id);
    expect(res.updateAmendment.days).toEqual(3);
  });

  it('should filter amendments by personId', async () => {
    const query = `{ allAmendments(filter: { personId:"${admin._id}" }){ id } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.allAmendments.length).toEqual(1);
  });

  it('should filter amendments by teamId', async () => {
    const query = `{ allAmendments(filter: { teamId:"${teamId}" }){ id } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.allAmendments.length).toEqual(1);
  });

  it('should delete an amendment', async () => {
    const query = `mutation{ deleteAmendment(id: "${amendment.id}"){ id }}`;
    const data = await runql(query).then(path(['data']));
    expect(data.deleteAmendment.id).toEqual(amendment.id);
  });
});

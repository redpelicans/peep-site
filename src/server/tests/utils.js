import { v4 as uuidv4 } from 'uuid';

export const waitFor = async fn => {
  return new Promise(resolve => {
    const tryFn = () => {
      try {
        fn();
        resolve();
      } catch (e) {
        setImmediate(tryFn);
      }
    };
    tryFn();
  });
};

export const initConfig = ctx =>
  ctx({
    config: {
      httpServer: { host: '0.0.0.0' },
      mongo: {
        url: process.env.CI ? 'mongodb://mongo:27017' : 'mongodb://localhost:27017',
        dbName: `test_${uuidv4()}`,
      },
      secretKey: '12345',
      sessionDuration: 1,
      isProduction: false,
      siteUrl: 'http://peep.com',
    },
  });

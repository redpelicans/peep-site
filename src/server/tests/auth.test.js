import { initResources } from 'jeto';
import { path } from 'ramda';
import got from 'got';
import { graphql } from 'graphql';
import initModels from '../init/models';
import initHttp from '../init/http';
import initRouter from '../init/router';
import initDatabase from '../init/database';
import initServices from '../services';
import initApollo from '../init/apollo';
import initDataLoaders from '../dataLoaders';
import initReactor from '../reactor';
import { ROLE } from '../models/roles';
import { STATUS } from '../models/statuses';
import { initConfig, waitFor } from './utils';
import { RequestNewPasswordEmail, InvitationEmail, UserConfirmedEmail } from '../emails';

let CTX;
let GCTX;
let runql;
let admin;
let userId;
let adminToken;
let inviteToken;
let inviteUrl;
let resetToken;

const initEmail = ctx => ctx({ email: { send: jest.fn() } });

jest.mock('../emails', () => ({
  InvitationEmail: jest.fn(),
  RequestNewPasswordEmail: jest.fn(),
  UserConfirmedEmail: jest.fn(),
}));

const initPostConfig = ctx =>
  ctx({
    ...ctx(),
    config: {
      ...ctx().config,
      siteUrl: ctx().httpServer.url,
    },
  });

describe('server | auth', () => {
  beforeAll(async () => {
    const resources = [
      initConfig,
      initDatabase,
      initModels,
      initDataLoaders,
      initServices,
      initApollo,
      initRouter,
      initHttp,
      initEmail,
      initReactor,
      initPostConfig,
    ];

    try {
      await initResources(resources).then(async ctx => {
        CTX = ctx;
      });
      const adminDoc = {
        firstname: 'admin',
        lastname: 'admin',
        password: 'password',
        email: 'admin@peep.fr',
        roles: [ROLE.admin],
        status: STATUS.active,
      };
      admin = await CTX().models.people.create(adminDoc);
      GCTX = {
        user: admin,
        ctx: CTX,
        models: CTX().models(admin, { authRequired: true }),
        dataLoaders: CTX().dataLoaders,
      };

      runql = async query => {
        const res = await graphql(CTX().apollo.schema, query, null, GCTX);
        if (res.errors) throw res.errors[0];
        return res;
      };
    } catch (err) {
      console.error(err);
      CTX()
        .mongo.database.dropDatabase()
        .then(() => CTX().httpServer.close())
        .then(() => CTX().mongo.close());
    }
  });

  afterAll(() => {
    return CTX()
      .mongo.database.dropDatabase()
      .then(() => CTX().httpServer.close())
      .then(() => CTX().mongo.close());
  });

  it('should login admin', async () => {
    const { token } = await got
      .post(`${CTX().httpServer.url}/api/auth/signIn`, { json: { email: admin.email, password: 'password' } })
      .json();
    adminToken = token;
    expect(adminToken).toBeDefined();
  });

  it('shouldnt send an email', async () => {
    CTX().email.send.mockClear();
    const query = `mutation { createUser(firstname: "firstname2", lastname: "lastname2", email: "email2", status: active , password:"1234" ){ id } }`;
    const res = await runql(query).then(path(['data']));
    userId = res.createUser.id;
    expect(res.createUser.id).toBeDefined();
    await waitFor(() => {
      expect(CTX().email.send).not.toHaveBeenCalled();
    });
  });

  it('should create a new user', async () => {
    const query = `mutation { createUser(firstname: "firstname", lastname: "lastname", email: "email", status: pending ){ id } } `;
    const res = await runql(query).then(path(['data']));
    userId = res.createUser.id;
    expect(res.createUser.id).toBeDefined();
    await waitFor(() => {
      expect(CTX().email.send).toHaveBeenCalled();
    });
  });

  it('should update user', async () => {
    CTX().email.send.mockClear();
    const query = `mutation { updateUser(id: "${userId}", firstname: "Firstname-NEW", status: pending ){ firstname } } `;
    const res = await runql(query).then(path(['data']));
    expect(res.updateUser.firstname).toEqual('FIRSTNAME-NEW');
    await waitFor(() => {
      expect(CTX().email.send).toHaveBeenCalled();
    });
  });

  it('should invite new user', async () => {
    CTX().email.send.mockClear();
    InvitationEmail.mockClear();
    InvitationEmail.mockReturnValue('COUCOU');
    const req = { headers: { authorization: `Bearer ${adminToken}` } };
    await CTX().services.api.run({ service: 'auth', method: 'invite', input: { id: userId } }, { req });
    await waitFor(() => {
      expect(CTX().email.send).toHaveBeenCalled();
      expect(InvitationEmail).toHaveBeenCalled();
    });
    expect(InvitationEmail.mock.calls[0][0].person._id.toString()).toEqual(userId);
    expect(InvitationEmail.mock.calls[0][0].author._id.toString()).toEqual(admin._id.toString());
    expect(InvitationEmail.mock.calls[0][0].sessionDuration).toBeDefined();
    expect(InvitationEmail.mock.calls[0][0].url).toBeDefined();
    const url = InvitationEmail.mock.calls[0][0].url;
    inviteToken = new URL(url).searchParams.get('id');
  });

  it('should confirm account', async () => {
    CTX().email.send.mockClear();
    UserConfirmedEmail.mockClear();
    const headers = { authorization: `Bearer ${inviteToken}` };
    const { user, token } = await got
      .post(`${CTX().httpServer.url}/api/auth/confirmAccount`, { headers, json: { password: 'user' } })
      .json();
    await waitFor(() => {
      expect(CTX().email.send).toHaveBeenCalled();
      expect(UserConfirmedEmail).toHaveBeenCalled();
    });
    expect(UserConfirmedEmail.mock.calls[0][0].user._id.toString()).toEqual(user._id);
    expect(UserConfirmedEmail.mock.calls[0][0].admin._id).toEqual(admin._id);
    expect(token).toBeDefined();
    expect(user.status).toEqual(STATUS.active);
    expect(user._id.toString()).toEqual(userId);
  });

  it('should login user', async () => {
    const { token } = await got
      .post(`${CTX().httpServer.url}/api/auth/signIn`, { json: { email: 'email', password: 'user' } })
      .json();
    expect(token).toBeDefined();
  });

  it('should set last Login', async () => {
    const {
      user: { lastLogin },
    } = await got
      .post(`${CTX().httpServer.url}/api/auth/signIn`, { json: { email: 'email', password: 'user' } })
      .json();
    expect(lastLogin).toBeDefined();
  });

  it('should request new password', async () => {
    CTX().email.send.mockClear();
    RequestNewPasswordEmail.mockClear();
    await CTX().services.api.run({ service: 'auth', method: 'requestNewPassword', input: { email: 'email' } }, {});
    await waitFor(() => {
      expect(CTX().email.send).toHaveBeenCalled();
    });
    expect(RequestNewPasswordEmail.mock.calls[0][0].person._id.toString()).toEqual(userId);
    expect(RequestNewPasswordEmail.mock.calls[0][0].sessionDuration).toBeDefined();
    expect(RequestNewPasswordEmail.mock.calls[0][0].url).toBeDefined();
    const url = InvitationEmail.mock.calls[0][0].url;
    resetToken = new URL(url).searchParams.get('id');
  });

  it('should reset password', async () => {
    const headers = { authorization: `Bearer ${resetToken}` };
    const { user, token } = await got
      .post(`${CTX().httpServer.url}/api/auth/resetPassword`, { headers, json: { password: 'resetPassword' } })
      .json();
    expect(token).toBeDefined();
    expect(user.status).toEqual(STATUS.active);
    expect(user._id.toString()).toEqual(userId);
  });

  it('should login user', async () => {
    const { token } = await got
      .post(`${CTX().httpServer.url}/api/auth/signIn`, { json: { email: 'email', password: 'resetPassword' } })
      .json();
    expect(token).toBeDefined();
  });

  it('should singUp user', async () => {
    InvitationEmail.mockClear();
    await got
      .post(`${CTX().httpServer.url}/api/auth/signUp`, {
        json: { firstname: 'firstname', lastname: 'lastname', email: 'signUp', password: 'user' },
      })
      .json();
    await waitFor(() => {
      expect(InvitationEmail).toHaveBeenCalled();
    });
    inviteUrl = InvitationEmail.mock.calls[0][0].url;
  });

  it('should confirm signup account', async () => {
    await got.get(inviteUrl, { followRedirect: false });
  });

  it('should login signUp user', async () => {
    const { token } = await got
      .post(`${CTX().httpServer.url}/api/auth/signIn`, { json: { email: 'signUp', password: 'user' } })
      .json();
    expect(token).toBeDefined();
  });
});

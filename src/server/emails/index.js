export const InvitationEmail = ({ siteUrl, url, person, sessionDuration }) => {
  return `
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <style>
      span {
        display: block;
        color:black;
       font-family:Helvetica Neue,Helvetica,Arial,sans-serif;
      }  
      p{
        color:black;
        font-family:Helvetica Neue,Helvetica,Arial,sans-serif;
      }      
      </style>
    </head>
    <body style="border-top: 5px solid rgb(205, 68, 54);"> 
    <div
    style="      
      text-align:center;      
    "
  >
  <div>
  <i class="fas fa-paper-plane"></i>
       </div>
       <span style="font-size:4.5vh;margin-top:5vh">Hello ${person.firstname}</span>
        <p >
        Congratulation, you are invited to create an account for Peep.
        </p>
        <p>
        click the link below to complete the process of confirming your account.        
        </p>
        <span style="margin-bottom:3vh">
        <a href="${url}">Confirm your account</a>
        </span>
        <span style="margin-bottom:5vh">
          Warning: link will only be usable for ${sessionDuration} hours
        </span>  
        <span style="font-size:1.5vh;color:grey">
        You're receiving this email because of your account on <a href="${siteUrl}" target="_blank">peep.com</a>
        </span>    
      </div> 
    </body>
  </html>
  `;
};

export const RequestNewPasswordEmail = ({ url, siteUrl, person, sessionDuration }) => {
  return `
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <style>
      span {
        display: block;
       font-family:Helvetica Neue,Helvetica,Arial,sans-serif;
      }        
      </style>
     
      
    </head>
    <body style="border-top: 5px solid rgb(205, 68, 54);"> 
    <div
    style="      
      text-align:center;      
    "
  >
  <div>
  <i class="fas fa-paper-plane"></i>
       </div>
       <span style="font-size:4.5vh;margin-top:5vh">Hello ${person.firstname}</span>
        <span>
          Click the link below to complete the process of setting your password
        </span>
        <span style="margin-bottom:3vh">
        <a href="${url}">Reset your password</a>
        </span>
        <span style="margin-bottom:5vh">
          Warning: link will only be usable ${sessionDuration} hour(s)
        </span>  
        <span style="font-size:1.5vh;color:grey">
        You're receiving this email because of your account on <a href="${siteUrl}" target="_blank">peep.com</a>
        </span>    
      </div>
 
    </body>
  </html>
               
  `;
};

export const UserConfirmedEmail = ({ siteUrl, user, admin }) => {
  return `
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <style>
      span {
        display: block;
        color:black;
       font-family:Helvetica Neue,Helvetica,Arial,sans-serif;
      }  
      p{
        color:black;
        font-family:Helvetica Neue,Helvetica,Arial,sans-serif;
      }      
      </style>
    </head>
    <body style="border-top: 5px solid rgb(205, 68, 54);"> 
    <div
    style="      
      text-align:center;      
    "
  >
  <div>
  <i class="fas fa-paper-plane"></i>
       </div>
       <span style="font-size:4.5vh;margin-top:5vh">Hello ${admin.firstname}</span>
        <p>
        User ${user.firstname} ${user.lastname} has confirmed his account
        </p>
        <span style="font-size:1.5vh;color:grey">
        You're receiving this email because of your account on <a href="${siteUrl}" target="_blank">peep.com</a>
        </span>    
      </div> 
    </body>
  </html>
  `;
};

export const TYPE = {
  vacation: 'vacation',
  dayOff: 'dayOff',
  unpaidLeave: 'unpaidLeave',
  sickLeave: 'sickLeave',
  workingDay: 'workingDay',
};

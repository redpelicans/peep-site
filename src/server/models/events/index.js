import { ApolloError } from 'apollo-server';
import { pick, dissoc, prop } from 'ramda';
import { ObjectID } from 'mongodb';
import { isManager, isAdmin, isMember } from '../roles';
import { TYPE } from './types';
import Model from '../model';

const NAME = 'events';

class EventModel extends Model {
  constructor(name, collection, user, models, options) {
    super(name, collection, user, models, options);
  }

  async loadAllForCompany({ from, to, personIds, teamId }) {
    const team = await this.models.teams.loadOne(teamId);
    if (!team) throw new ApolloError('Unknown team', 404);
    if (!isAdmin(this.user) && !isMember(team, this.user)) throw new ApolloError('Unauthorized', 403);
    const filter = {
      $or: [
        { teamId: ObjectID(teamId), type: { $in: [TYPE.dayOff, TYPE.workingDay] } },
        { personId: { $in: personIds }, type: { $in: [TYPE.vacation, TYPE.unpaidLeave, TYPE.sickLeave] } },
      ],
    };
    if (from) filter.from = { $gte: from };
    if (to) filter.to = { $lte: to };

    return this.collection.find(filter).toArray();
  }

  async loadAllForCalendar({ from, to, personIds, teamId }) {
    const team = await this.models.teams.loadOne(teamId);
    if (!team) throw new ApolloError('Unknown team', 404);
    if (!isAdmin(this.user) && !isMember(team, this.user)) throw new ApolloError('Unauthorized', 403);
    const filter = {
      $or: [
        { teamId: ObjectID(teamId), type: { $in: [TYPE.dayOff, TYPE.workingDay] } },
        { personId: { $in: personIds }, type: { $in: [TYPE.vacation, TYPE.unpaidLeave, TYPE.sickLeave] } },
      ],
    };
    if (from) filter.from = { $gte: from };
    if (to) filter.to = { $lte: to };

    return this.collection.find(filter).toArray();
  }

  async delete(id) {
    const version = await this.loadOne(id);
    if (!version) throw new ApolloError('Unknown event', 404);
    if (isAdmin(this.user) || this.user._id.equals(version.personId))
      return this.collection.deleteOne({ _id: new ObjectID(id) });
    else if (version.teamId) {
      const team = await this.models.teams.loadOne(version.teamId);
      if (isManager(team, this.user)) return this.collection.deleteOne({ _id: new ObjectID(id) });
    }
    throw new ApolloError('Unauthorized', 403);
  }

  async create(event) {
    this.checkAuth();
    const team = await this.models.teams.loadOne(event.teamId);
    if (!isAdmin(this.user) && !isMember(team, this.user) && !isManager(team, this.user))
      throw new ApolloError('Unauthorized', 403);
    const insertedId = await this.insertOne(this._create(event, this.user));
    const res = await this.loadOne(insertedId);
    return res;
  }

  async updateOne(event) {
    this.checkAuth();
    const existingEvent = await this.loadOne(event._id);
    if (!existingEvent) throw new ApolloError(`Unknown event ${event._id}`, 404);
    const team = await this.models.teams.loadOne(event.teamId);
    if (
      !isAdmin(this.user) &&
      !isManager(team, this.user) &&
      (!isMember(team, this.user) || !existingEvent.personId.equals(this.user._id))
    )
      throw new ApolloError('Unauthorized', 403);
    await this.collection.updateOne(
      { _id: ObjectID(event._id) },
      { $set: this._update({ ...pick(['from', 'to', 'type', 'teamId'], existingEvent), ...event }) },
    );
    return this.loadOne(event._id);
  }

  _make(event) {
    const version = { ...event };
    if (event.personId) version.personId = ObjectID(event.personId);
    if (event.teamId) version.teamId = ObjectID(event.teamId);
    return version;
  }

  _create(event) {
    const version = {
      ...this._make(event, this.user),
      createdAt: new Date(),
      createdBy: prop('_id', this.user),
    };
    return version;
  }

  _update(event) {
    const version = {
      ...this._make(event, this.user),
      updatedAt: new Date(),
      updatedBy: prop('_id', this.user),
    };
    return dissoc('_id', version);
  }

  _makeFilter({ type, from, to, personId, teamId, types } = {}) {
    const filter = {};
    if (from) filter.from = { $gte: from };
    if (to) filter.to = { $lte: to };
    if (personId) filter.personId = ObjectID(personId);
    if (teamId) filter.teamId = ObjectID(teamId);
    if (type) filter.type = type;
    if (types) filter.type = { $in: types };
    return filter;
  }
}

export default database => {
  const collection = database.collection(NAME);
  return [
    NAME,
    collection,
    (user, models, options) => new EventModel(NAME, collection, user, models, options),
    {
      indexes: [[{ from: 1 }], [{ to: 1 }]],
    },
  ];
};

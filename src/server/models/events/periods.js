import { addHours, startOfDay, endOfDay } from 'date-fns';

export const PERIOD = {
  DAY: 'day',
  PM: 'pm',
  AM: 'am',
};

export const makePeriod = event => {
  switch (event.period) {
    case PERIOD.PM:
      return [addHours(startOfDay(event.from), 12), endOfDay(event.from)];
    case PERIOD.AM:
      return [startOfDay(event.from), addHours(startOfDay(event.from), 12)];
    default:
      return [startOfDay(event.from), endOfDay(event.from)];
  }
};

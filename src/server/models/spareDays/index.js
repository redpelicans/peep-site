import { ApolloError } from 'apollo-server';
import { isWeekend, eachDayOfInterval, startOfDay, endOfDay } from 'date-fns';
import { either, reduce, reject, pick, dissoc, prop, not, pluck, head, compose, identity, sortBy } from 'ramda';
import { ObjectID } from 'mongodb';
import Model from '../model';
import { isAdmin, hasRight, RIGHT, hasSpareDaysRight } from '../roles';
import { mdy } from '../../../lib/models/utils';

const NAME = 'spareDays';

class SpareDayModel extends Model {
  constructor(name, collection, user, models, options) {
    super(name, collection, user, models, options);
  }

  async loadForPeriod({ from, to, team }) {
    const filter = {};
    filter.from = { $gte: new Date(from), $lte: new Date(to) };
    filter.$or = [{ teamId: ObjectID(team._id) }, { country: team.country }];
    return this.collection.find(filter).toArray();
  }

  async workingDaysOfPeriod({ from, to, teamId }) {
    const team = await this.models.teams.loadOne(teamId);
    if (!team) throw new ApolloError(`Unknown team ${teamId}`, 404);
    const spareDays = await this.loadForPeriod({from, to, team});

    const hSpareDays = reduce(
      (acc, day) => {
        acc[mdy(day.from)] = day;
        return acc;
      },
      {},
      spareDays,
    );
    const isSpareDay = date => hSpareDays[mdy(date)];
    const workingDays = reject(either(isWeekend, isSpareDay), eachDayOfInterval({ start: from, end: to }));
    return {
      from,
      to,
      teamId,
      workingDays: workingDays.length,
    };
  }

  async checkSpareDayUniqueness({ from, country, teamId }) {
    if (!isAdmin(this.user) && !hasRight(RIGHT.spareDays)) throw new ApolloError('Unauthorized', 403);
    if (country)
      return this.collection.findOne({ country, from: { $gte: startOfDay(from), $lte: endOfDay(from) } }).then(not);

    return this.collection
      .findOne({ teamId: ObjectID(teamId), from: { $gte: startOfDay(from), $lte: endOfDay(from) } })
      .then(not);
  }

  async getSpareDaysCountries() {
    if (!isAdmin(this.user) && !hasRight(RIGHT.spareDays)) throw new ApolloError('Unauthorized', 403);
    return await this.collection.distinct('country');
  }

  async getSpareDaysYears() {
    if (!isAdmin(this.user) && !hasRight(RIGHT.spareDays)) throw new ApolloError('Unauthorized', 403);
    const result = await this.collection
      .aggregate([
        {
          $project: {
            year: { $year: '$from' },
          },
        },
        {
          $group: {
            _id: null,
            distinctDate: { $addToSet: { year: '$year' } },
          },
        },
      ])
      .toArray();
    return compose(sortBy(identity), pluck('year'), prop('distinctDate'), head)(result);
  }

  async create(event) {
    if (!isAdmin(this.user) && !hasRight(RIGHT.spareDays)) throw new ApolloError('Unauthorized', 403);
    if (!(await this.checkSpareDayUniqueness(event))) throw new ApolloError('A spare day must be unique! ', 400);
    if (!event.country && !event.teamId)
      throw new ApolloError('A spare day must have a country or a teamId prop !', 400);
    const insertedId = await this.insertOne(this._create(event, this.user));
    const res = await this.loadOne(insertedId);
    return res;
  }

  async updateOne(event) {
    if (!isAdmin(this.user) && !hasRight(RIGHT.spareDays)) throw new ApolloError('Unauthorized', 403);
    const existingEvent = await this.loadOne(event._id);
    if (!existingEvent) throw new ApolloError(`Unknown spare day ${event._id}`, 404);
    await this.collection.updateOne(
      { _id: ObjectID(event._id) },
      { $set: this._update({ ...pick(['from', 'to', 'teamId'], existingEvent), ...event }) },
    );
    return this.loadOne(event._id);
  }

  async delete(id) {
    const spareDay = await this.loadOne(id);
    if (!spareDay) throw new ApolloError('Unknown document', 404);
    if (!(isAdmin(this.user) || hasSpareDaysRight(this.user))) throw new ApolloError('Unauthorized', 403);
    return this.collection.deleteOne({ _id: new ObjectID(id) });
  }

  _make(event) {
    const version = { ...event };
    if (event.teamId) version.teamId = ObjectID(event.teamId);
    if (event.country && !version.teamId) version.country = event.country;
    return version;
  }

  _create(event) {
    const version = {
      ...this._make(event, this.user),
      createdAt: new Date(),
      createdBy: prop('_id', this.user),
    };
    return version;
  }

  _update(event) {
    const version = {
      ...this._make(event, this.user),
      updatedAt: new Date(),
      updatedBy: prop('_id', this.user),
    };
    return dissoc('_id', version);
  }

  _makeFilter({ country, label, from, to, teamId } = {}) {
    const filter = {};
    if (from) filter.from = { $gte: from };
    if (to) filter.to = { $lte: to };
    if (teamId && country) filter.$or = [{ teamId: ObjectID(teamId) }, { country }];
    else if (teamId) filter.teamId = ObjectID(teamId);
    else if (country) filter.country = country;
    if (label) filter.label = new RegExp(label, 'i');
    return filter;
  }
}

export default database => {
  const collection = database.collection(NAME);
  return [
    NAME,
    collection,
    (user, models, options) => new SpareDayModel(NAME, collection, user, models, options),
    {
      indexes: [[{ from: 1 }], [{ to: 1 }]],
    },
  ];
};

import { ObjectID } from 'mongodb';
import { MEMBER_ROLE, isManager, RIGHT, hasTeamsCreateRight, hasRights, isAuthor, hasSpareDaysRight } from '../roles';

const user1 = ObjectID();
const user2 = ObjectID();
const user3 = ObjectID();
const team = {
  _id: 'team1',
  createdBy: ObjectID('5f2c9ac7747e3317a7ca1fe6'),
  members: [
    {
      personId: user1,
      role: MEMBER_ROLE.headWorker,
    },
    {
      personId: user2,
      role: MEMBER_ROLE.manager,
    },
    {
      personId: user3,
      role: MEMBER_ROLE.worker,
    },
  ],
};

const user4 = { _id: ObjectID('5f2c9ac7747e3317a7ca1fe6'), rights: [RIGHT.teamsCreate] };
const user5 = { _id: ObjectID(), rights: [RIGHT.spareDays] };
const user6 = { _id: ObjectID(), rights: [RIGHT.spareDays, RIGHT.teamsCreate] };
const user7 = { _id: ObjectID() };

describe('models | roles', () => {
  it('should user1 be a manager', () => {
    expect(isManager(team, { _id: user1 })).toBeTruthy();
  });

  it('should user2 be a manager', () => {
    expect(isManager(team, { _id: user2 })).toBeTruthy();
  });

  it('should user3 not be a manager', () => {
    expect(isManager(team, { _id: user3 })).toBeFalsy();
  });
  it('should check if user4 have teamsCreate right', () => {
    expect(hasTeamsCreateRight(user4)).toBeTruthy();
  });
  it('Should check if user5 have no teamsCreate right  ', () => {
    expect(hasTeamsCreateRight(user5)).toBeFalsy();
  });
  it('should check if user6 have teamsCreate and spareDays right', () => {
    expect(hasRights([RIGHT.teamsCreate, RIGHT.spareDays], user6)).toBeTruthy();
  });
  it('should user7 have no right', () => {
    expect(hasRights([RIGHT.teamsCreate, RIGHT.spareDays], user7)).toBeFalsy();
  });
  it('should check if team is created by user4', () => {
    expect(isAuthor(team, user4)).toBeTruthy();
  });
  it('should check if team is created by user5', () => {
    expect(isAuthor(team, user5)).toBeFalsy();
  });
  it('should check if user5 have spareDays right', () => {
    expect(hasSpareDaysRight(user5)).toBeTruthy();
  });
});

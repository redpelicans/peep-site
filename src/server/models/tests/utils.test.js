import { getActiveParticipations } from '../utils';
import { pluck } from 'ramda';
import { ObjectID } from 'mongodb';

const user1 = ObjectID();
const user2 = ObjectID();
const user3 = ObjectID();
const teams = [
  {
    _id: 'team1',
    members: [
      {
        personId: user1,
        activityPeriods: [
          { expirationDate: new Date(2019, 2, 1) },
          { inceptionDate: new Date(2020, 1, 1), expirationDate: new Date(2020, 2, 1) },
          { inceptionDate: new Date(2020, 4, 1), expirationDate: new Date(2020, 5, 1) },
        ],
      },
      {
        personId: user2,
        activityPeriods: [
          {
            inceptionDate: null,
            expirationDate: null,
          },
        ],
      },
    ],
  },
  {
    _id: 'team2',
    members: [
      {
        personId: user1,
      },
    ],
  },
];

describe('models | utils', () => {
  it('should select active participations for user1', () => {
    const now = new Date(2020, 1, 2);
    const res = getActiveParticipations(now, user1, teams);
    expect(pluck('teamId', res)).toEqual(['team1', 'team2']);
  });

  it('should select active participations for user2', () => {
    const now = new Date(2020, 1, 2);
    const res = getActiveParticipations(now, user2, teams);
    expect(pluck('teamId', res)).toEqual(['team1']);
  });

  it('should select active participations for user3', () => {
    const now = new Date(2020, 1, 2);

    const res = getActiveParticipations(now, user3, teams);
    expect(res.length).toEqual(0);
  });
});

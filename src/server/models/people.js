import { ApolloError } from 'apollo-server';
import { ObjectID } from 'mongodb';
import { compose, pluck, propOr, reduce, prop, dissoc, slice, isNil } from 'ramda';
import njwt from 'njwt';
import Model from './model';
import { ROLE, MEMBER_ROLE } from './roles';
import { STATUS } from './statuses';
import { cryptPassword } from '../utils';
import { isLoggedUser, isAdmin } from './roles';
import debug from '../debug';

const NAME = 'people';

const getToken = slice(7, Infinity);

export class PersonModel extends Model {
  constructor(name, collection, user, models, options) {
    super(name, collection, user, models, options);
  }

  loadByEmail(email) {
    return this.collection.findOne({ email: email.toLowerCase() });
  }

  checkEmailUniqueness(email) {
    return this.collection.findOne({ email: email.toLowerCase() }, { projection: { _id: 1 } }).then(isNil);
  }

  loadFromToken(token, secret) {
    if (!token) return Promise.resolve();
    return new Promise(resolve => {
      njwt.verify(getToken(token), secret, (err, njwtoken) => {
        if (err) {
          debug.error(err);
          return resolve();
        }
        const id = njwtoken.body.sub;
        this.loadOne(id).then(resolve);
      });
    });
  }

  async search({ match }) {
    if (!match) return [];
    if (match.length < 4) throw new ApolloError('Wrong match value', 400);
    this.checkAuth();
    const query = {
      $or: [
        { firstname: { $regex: match, $options: 'i' } },
        { lastname: { $regex: match, $options: 'i' } },
        { email: { $regex: match, $options: 'i' } },
      ],
    };
    const options = { projection: { firstname: 1, lastname: 1, email: 1 } };

    return this.collection.find(query, options).sort({ firstname: 1, lastname: 1 }).toArray();
  }

  async create(user) {
    this.checkIsAdmin();
    const existingPerson = await this.loadByEmail(user.email);
    if (existingPerson) throw new ApolloError(`${user.email} already exists`, 400);
    const insertedId = await this.insertOne(this._create(user));
    return this.loadOne(insertedId);
  }

  async updateOne(user) {
    const existingPerson = await this.loadOne(user._id);
    if (!existingPerson) throw new ApolloError(`Unknown user ${user._id}`, 404);
    if (!isAdmin(this.user) && !isLoggedUser(existingPerson, this.user)) throw new ApolloError('Unauthorized', 403);
    await this.collection.updateOne({ _id: ObjectID(user._id) }, { $set: dissoc('_id', this._update(user)) });
    return this.loadOne(user._id);
  }

  async _makeFilter({ q, status } = {}) {
    const filter = {};
    if (q)
      filter.$or = [
        { firstname: { $regex: q, $options: 'i' } },
        { lastname: { $regex: q, $options: 'i' } },
        { email: { $regex: q, $options: 'i' } },
      ];
    if (status) filter.status = status;
    if (isAdmin(this.user)) return filter;

    const teams = await this.models.teams.collection
      .find({
        'members.role': { $in: [MEMBER_ROLE.headWorker, MEMBER_ROLE.manager] },
        'members.personId': this.user._id,
      })
      .toArray();
    const getMemberIds = compose(pluck('personId'), propOr([], 'members'));
    const userIds = reduce((acc, team) => [...acc, ...getMemberIds(team)], [], teams);
    filter._id = { $in: userIds };
    return filter;
  }

  _make(user) {
    const version = {
      ...user,
      status: user.status || STATUS.inactive,
      roles: user.roles || [ROLE.user],
    };
    if (user.lastname) version.lastname = user.lastname.toUpperCase();
    if (user.firstname) version.firstname = user.firstname.toUpperCase();
    if (user.email) version.email = user.email.toLowerCase();
    if (user.password) version.password = cryptPassword(user.password);

    return version;
  }

  _create(user) {
    const version = {
      ...this._make(user),
      createdAt: new Date(),
      createdBy: prop('_id', this.user),
    };
    return version;
  }

  _update(user) {
    const version = {
      ...user,
      updatedAt: new Date(),
      updatedBy: prop('_id', this.user),
    };

    if (user.lastname) version.lastname = user.lastname.toUpperCase();
    if (user.firstname) version.firstname = user.firstname.toUpperCase();
    if (user.email) version.email = user.email.toLowerCase();
    if (user.password) version.password = cryptPassword(user.password);
    return dissoc('_id', version);
  }
}

export default database => {
  const collection = database.collection(NAME);
  return [
    NAME,
    collection,
    (user, models, options) => new PersonModel(NAME, collection, user, models, options),
    {
      indexes: [[{ email: 1 }, { unique: true }]],
    },
  ];
};

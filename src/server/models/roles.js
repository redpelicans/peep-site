import { ObjectID } from 'mongodb';
import { curry, find, contains, propOr, isEmpty, difference } from 'ramda';

export const ROLE = {
  admin: 'admin',
  api: 'api',
  user: 'user',
};

export const MEMBER_ROLE = {
  worker: 'worker',
  headWorker: 'headWorker',
  observer: 'observer',
  manager: 'manager',
};

export const RIGHT = {
  spareDays: 'spareDays',
  teamsCreate: 'teamsCreate',
};

export const isMember = (team, user) => find(({ personId }) => ObjectID(personId).equals(user._id), team.members || []);
export const hasRights = curry((rights, user) => isEmpty(difference(rights || [], propOr([], 'rights', user))));
export const hasRight = right => hasRights([right]);
export const isLoggedUser = (user1, user2) => user1._id.equals(user2._id);
export const isAdmin = user =>
  contains(ROLE.admin, propOr([], 'roles', user)) || contains(ROLE.api, propOr([], 'roles', user));
export const isManager = (team, user) =>
  find(
    ({ personId, role }) =>
      ObjectID(personId).equals(user._id) && (role === MEMBER_ROLE.headWorker || role === MEMBER_ROLE.manager),
    team.members || [],
  );

export const hasSpareDaysRight = hasRight(RIGHT.spareDays);
export const hasTeamsCreateRight = hasRight(RIGHT.teamsCreate);
export const isAuthor = (obj, user) => obj?.createdBy?.equals(user._id);

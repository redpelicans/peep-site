import { propEq } from 'ramda';

export const STATUS = {
  active: 'active',
  inactive: 'inactive',
  pending: 'pending',
};

export const isActive = propEq('status', STATUS.active);
export const isInActive = propEq('status', STATUS.inactive);
export const isPending = propEq('status', STATUS.pending);

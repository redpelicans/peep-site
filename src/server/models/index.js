import { toPairs, reduce, map } from 'ramda';
import Person from './people';
import Team from './teams';
import Event from './events';
import SpareDay from './spareDays';
import Amendment from './amendments';

const createIndexes = (collection, indexes = []) =>
  reduce((acc, [spec, options]) => acc.then(() => collection.createIndex(spec, options)), Promise.resolve(), indexes);

const providers = [Person, Team, Event, SpareDay, Amendment];

export default async (db, apiUser) => {
  const models = await reduce(
    (acc, provider) => {
      const [name, collection, modelFactory, { indexes } = {}] = provider(db);
      return acc.then(res => createIndexes(collection, indexes).then(() => ({ ...res, [name]: modelFactory })));
    },
    Promise.resolve({}),
    providers,
  );
  const res = (user, options) => map(modelFactory => modelFactory(user, res, options), models);
  reduce(
    (acc, [name, modelFactory]) => {
      acc[name] = modelFactory(apiUser, res);
      return acc;
    },
    res,
    toPairs(models),
  );
  return res;
};

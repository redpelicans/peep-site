import { ApolloError } from 'apollo-server';
import { difference, pluck, compose, values, propOr, curry, reject, dissoc, prop, map, reduce, isNil } from 'ramda';
import { ObjectID } from 'mongodb';
import Model from './model';
import { isMember, isAuthor, isManager, isAdmin, hasTeamsCreateRight } from './roles';
import { getActiveParticipations } from './utils';
import { getActiveWorkers } from '../../lib/models/teams';
import { isCompany } from '../../lib/models/teams';
import { event2Days, events2Days } from '../../lib/models/days';
import { TYPE } from './events/types';

const NAME = 'teams';

class TeamModel extends Model {
  constructor(name, collection, user, models, options) {
    super(name, collection, user, models, options);
  }

  async loadAllEvents({ from, to, teamId }) {
    const team = await this.models.teams.loadOne(teamId);
    if (!team) throw new ApolloError('Unknown team', 404);
    if (!isAdmin(this.user) && !isMember(team, this.user)) throw new ApolloError('Unauthorized', 403);
    if (isCompany(team)) return this.loadAllCompanyEvents({ from, to, team });
    return this.loadAllProjectEvents({ from, to, team });
  }

  async loadAllCompanyEvents({ from, to, team }) {
    const members = getActiveWorkers(team, from, to);
    const personIds = map(member => member.personId, members);
    const filter = {
      from: { $gte: from },
      to: { $lte: to },
      personId: { $in: personIds },
      type: { $in: [TYPE.workingDay, TYPE.vacation, TYPE.unpaidLeave, TYPE.sickLeave] },
    };
    return this.models.events.collection.find(filter).toArray();
  }

  loadAllProjectEvents({ from, to, team }) {
    const members = getActiveWorkers(team, from, to);
    const personIds = map(member => member.personId, members);
    const filter = {
      from: { $gte: from },
      to: { $lte: to },
      $or: [
        { teamId: team._id, type: { $in: [TYPE.dayOff, TYPE.workingDay] } },
        { personId: { $in: personIds }, type: { $in: [TYPE.vacation, TYPE.unpaidLeave, TYPE.sickLeave] } },
      ],
    };
    return this.models.events.collection.find(filter).toArray();
  }

  async getProjectWorkingDaysStats({ teamId, from, to }) {
    const team = await this.collection.findOne({ _id: ObjectID(teamId) });
    if (!team) throw new ApolloError(`Unknown team ${teamId}`, 404);
    return this.models.amendments.getStatsForTeam({ team, from, to });
  }

  async getCompanyWorkingDaysStats({ teamId, from, to }) {
    const team = await this.collection.findOne({ _id: ObjectID(teamId) });
    if (!team) throw new ApolloError(`Unknown team ${teamId}`, 404);
    const members = getActiveWorkers(team, from, to);
    const personIds = map(member => ObjectID(member.personId), members);
    const query = {
      personId: { $in: personIds },
      type: { $in: [TYPE.vacation, TYPE.unpaidLeave, TYPE.sickLeave] },
      from: { $lte: to },
      to: { $gte: from },
    };

    const [people, events, spareDays] = await Promise.all([
      this.models.people.collection.find({ _id: { $in: personIds } }, { projection: { password: 0 } }).toArray(),
      this.models.events.collection.find(query).toArray(),
      this.models.spareDays.loadForPeriod({ from, to, team }),
    ]);

    const hpersons = reduce(
      (acc, event) => {
        if (!acc[event.personId])
          acc[event.personId] = { [TYPE.vacation]: [], [TYPE.unpaidLeave]: [], [TYPE.sickLeave]: [] };
        acc[event.personId][event.type].push(event);
        return acc;
      },
      {},
      events,
    );
    const getEvents = (type, person) => hpersons[person._id]?.[type];

    return map(
      person => ({
        team,
        worker: person,
        from,
        to,
        workingDaysInPeriod: event2Days({ from, to }, { spareDays, from, to }).count, // TODO: manage inception, expiration Dates
        vacationDays: events2Days(getEvents(TYPE.vacation, person), { spareDays, from, to }).count,
        unpaidLeaveDays: events2Days(getEvents(TYPE.unpaidLeave, person), { spareDays, from, to }).count,
        sickLeaveDays: events2Days(getEvents(TYPE.sickLeave, person), { spareDays, from, to }).count,
      }),
      people,
    );
  }

  async getProjectWorkingDays(team, from, to, personId) {
    const query = {
      teamId: team._id,
      personId: ObjectID(personId),
      type: TYPE.workingDay,
      from: { $lte: to },
      to: { $gte: from },
    };
    const events = await this.models.events.collection.find(query).toArray();
    const spareDays = await this.models.spareDays.loadForPeriod({ from, to, team });
    return events2Days(events, { spareDays, from, to }).count;
  }

  loadByName(name) {
    return this.collection.findOne({ name });
  }

  checkTeamUniqueness(name) {
    if (!isAdmin(this.user) && !hasTeamsCreateRight(this.user)) throw new ApolloError('Unauthorized', 403);
    return this.collection.findOne({ name }, { projection: { _id: 1 } }).then(isNil);
  }
  async create(team) {
    const existingTeam = await this.collection.findOne({ name: team.name });
    if (existingTeam) throw new ApolloError(`${team.name} already exists`, 400);
    if (!isAdmin(this.user) && (!isManager(team, this.user) || !hasTeamsCreateRight(this.user)))
      throw new ApolloError('Unauthorized', 403);
    const version = this._create(team);
    this._checkMembers(version);
    const insertedId = await this.insertOne(version);
    return this.loadOne(insertedId);
  }

  async updateOne(team) {
    const existingTeam = await this.loadOne(team._id);
    if (!existingTeam) throw new ApolloError(`Unknown team ${team._id}`, 404);
    if (!isAdmin(this.user) && !isManager(existingTeam, this.user)) throw new ApolloError('Unauthorized', 403);
    await this.collection.updateOne({ _id: ObjectID(team._id) }, { $set: this._update(team) });
    return this.loadOne(team._id);
  }

  async removeMemberFromAllTeams(id) {
    this.checkIsAdmin();
    const personId = ObjectID(id);
    const existingTeams = await this.collection.find({ 'members.personId': personId }).toArray();

    const remove = curry((personId, team) => {
      const newTeam = {
        ...team,
        members: reject(member => member.personId.equals(personId), team.members),
      };
      return this.updateOne(newTeam);
    });

    return Promise.all(map(remove(personId), existingTeams));
  }

  async _checkMembers(team) {
    const personIds = compose(map(prop('personId')), propOr([], 'members'))(team);
    const res = await this.models.people.collection
      .find({ _id: { $in: personIds } }, { projection: { _id: 1 } })
      .toArray();
    const existingPersons = pluck('_id', res);
    const diff = difference(personIds, existingPersons);
    if (diff.length) throw new ApolloError('Unknow people ${diff}');
  }

  async delete(id) {
    const team = await this.loadOne(id);
    if (!team) throw new ApolloError('Unknown document', 404);
    if (!(isAdmin(this.user) || (isManager(team, this.user) && isAuthor(team, this.user))))
      throw new ApolloError('Unauthorized', 403);
    return this.collection.deleteOne({ _id: new ObjectID(id) });
  }

  _make(team) {
    const version = { ...team };
    if (team.members) {
      const hmembers = reduce(
        (acc, member) => {
          acc[member.personId] = { ...member, personId: ObjectID(member.personId) };
          return acc;
        },
        {},
        team.members,
      );
      version.members = values(hmembers);
    }
    return version;
  }

  _create(team) {
    const version = {
      ...this._make(team),
      createdAt: new Date(),
      createdBy: prop('_id', this.user),
    };
    return version;
  }

  _update(team) {
    const version = {
      ...this._make(team),
      updatedAt: new Date(),
      updatedBy: prop('_id', this.user),
    };
    return dissoc('_id', version);
  }

  async _makeFilter({ q, country, userId } = {}) {
    const filter = {};
    if (q) filter.$or = [{ name: { $regex: q, $options: 'i' } }];
    if (country) filter.country = country;
    if (isAdmin(this.user) && userId) {
      const teams = await this.collection
        .find({ 'members.personId': ObjectID(userId) }, { projection: { members: 1 } })
        .toArray();
      const activeTeams = getActiveParticipations(new Date(), ObjectID(userId), teams);
      filter._id = { $in: pluck('teamId', activeTeams) };
    } else if (!isAdmin(this.user)) {
      const teams = await this.collection
        .find({ 'members.personId': this.user._id }, { projection: { members: 1 } })
        .toArray();
      const activeTeams = getActiveParticipations(new Date(), this.user._id, teams);
      filter._id = { $in: pluck('teamId', activeTeams) };
    }
    return filter;
  }
}

export default database => {
  const collection = database.collection(NAME);
  return [
    NAME,
    collection,
    (user, models, options) => new TeamModel(NAME, collection, user, models, options),
    {
      indexes: [[{ name: 1 }], [{ 'members.personId': 1 }]],
    },
  ];
};

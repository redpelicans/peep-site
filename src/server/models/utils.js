import { reduce, compose, filter, find, any } from 'ramda';

const isActivePeriod = now => ({ inceptionDate, expirationDate }) =>
  (!inceptionDate || now > inceptionDate) && (!expirationDate || expirationDate > now);
const hasAnActivePeriod = now => ({ member: { activityPeriods } = {} }) =>
  !activityPeriods || any(isActivePeriod(now), activityPeriods);
const selectMember = (userId, members) => find(member => member.personId.equals(userId), members);

export const getActiveParticipations = (now, userId, teams) =>
  compose(
    x => x || [],
    filter(hasAnActivePeriod(now)),
    reduce((acc, { _id, members }) => {
      const member = selectMember(userId, members);
      if (member) return [...acc, { teamId: _id, member }];
      return acc;
    }, []),
  )(teams);

import { ApolloError } from 'apollo-server';
import startOfYear from 'date-fns/startOfYear';
import endOfYear from 'date-fns/endOfYear';
import { zip, compose, values, reduce, map, pick, dissoc, prop } from 'ramda';
import { ObjectID } from 'mongodb';
import { isAdmin, isManager } from './roles';
import { getActiveWorkers } from '../../lib/models/teams';
import Model from './model';

const NAME = 'amendments';

class AmendmentModel extends Model {
  constructor(name, collection, user, models, options) {
    super(name, collection, user, models, options);
  }

  async getAmendmentsUsers() {
    const personIds = await this.collection.distinct('personId');
    return await this.models.people.collection.find({ _id: { $in: personIds } }).toArray();
  }

  async getStatsForTeam({ team, from, to }) {
    const members = getActiveWorkers(team, from, to);
    const personIds = map(member => ObjectID(member.personId), members);
    const [persons, amendments] = await Promise.all([
      this.models.people.collection.find({ _id: { $in: personIds } }, { projection: { password: 0 } }).toArray(),
      this.collection.find({ teamId: ObjectID(team._id), personId: { $in: personIds } }).toArray(),
    ]);

    const hpersons = reduce(
      (acc, person) => {
        acc[person._id] = person;
        return acc;
      },
      {},
      persons,
    );

    const lastAmendments = compose(
      values,
      reduce((acc, a) => {
        if (a.from > to) return acc;
        if (!acc[a.personId]) acc[a.personId] = a;
        else if (acc[a.personId].from < a.from) acc[a.personId] = a;
        return acc;
      }, {}),
    )(amendments);

    const [startYear, endYear] = [startOfYear(from), endOfYear(to)];
    const yearPromises = Promise.all(
      map(personId => this.models.teams.getProjectWorkingDays(team, startYear, endYear, personId), personIds),
    );
    const amendmentsPromises = Promise.all(
      map(
        amendment => this.models.teams.getProjectWorkingDays(team, amendment.from, amendment.to, amendment.personId),
        lastAmendments,
      ),
    );
    const [yearWD, amendmentWD] = await Promise.all([yearPromises, amendmentsPromises]);

    const hamendmentWD = reduce(
      (acc, [lastAmendment, amendmentWorkingDays]) => {
        acc[lastAmendment.personId] = { amendmentWorkingDays, lastAmendment };
        return acc;
      },
      {},
      zip(amendments, amendmentWD),
    );

    return reduce(
      (acc, [personId, workingDays]) => {
        const e = {
          team,
          from,
          to,
          worker: hpersons[personId],
          ...hamendmentWD[personId],
          currentYearWorkingDays: workingDays,
        };
        acc.push(e);
        return acc;
      },
      [],
      zip(personIds, yearWD),
    );
  }

  async getAmendmentsTeams() {
    const teamIds = await this.collection.distinct('teamId');
    return await this.models.teams.collection.find({ _id: { $in: teamIds } }).toArray();
  }
  async delete(id) {
    const version = await this.loadOne(id);
    if (!version) throw new ApolloError('Unknown event', 404);
    const team = await this.models.teams.collection.findOne({ _id: ObjectID(version.teamId) });
    if (isAdmin(this.user) || isManager(team, this.user)) return this.collection.deleteOne({ _id: new ObjectID(id) });
    throw new ApolloError('Unauthorized', 403);
  }

  async create(amendment) {
    const team = await this.models.teams.collection.findOne({ _id: ObjectID(amendment.teamId) });
    if (!team) throw new ApolloError(`Unknown team ${team._id}`, 404);
    if (!isAdmin(this.user) && !isManager(team, this.user)) throw new ApolloError('Unauthorized', 403);
    const insertedId = await this.insertOne(this._create(amendment, this.user));
    const res = await this.loadOne(insertedId);
    return res;
  }

  async updateOne(amendment) {
    const team = await this.models.teams.collection.findOne({ _id: ObjectID(amendment.teamId) });
    if (!team) throw new ApolloError(`Unknown team ${team._id}`, 404);
    if (!isAdmin(this.user) && !isManager(team, this.user)) throw new ApolloError('Unauthorized', 403);
    const existingAmendment = await this.loadOne(amendment._id);
    if (!existingAmendment) throw new ApolloError(`Unknown event ${amendment._id}`, 404);
    await this.collection.updateOne(
      { _id: ObjectID(amendment._id) },
      {
        $set: this._update({ ...pick(['from', 'to', 'type', 'teamId', 'personId'], existingAmendment), ...amendment }),
      },
    );
    return this.loadOne(amendment._id);
  }

  _make(amendment) {
    const version = { ...amendment };
    if (amendment.personId) version.personId = ObjectID(amendment.personId);
    if (amendment.teamId) version.teamId = ObjectID(amendment.teamId);
    return version;
  }

  _create(amendment) {
    const version = {
      ...this._make(amendment, this.user),
      createdAt: new Date(),
      createdBy: prop('_id', this.user),
    };
    return version;
  }

  _update(amendment) {
    const version = {
      ...this._make(amendment, this.user),
      updatedAt: new Date(),
      updatedBy: prop('_id', this.user),
    };
    return dissoc('_id', version);
  }

  async _makeFilter({ personId, teamId } = {}) {
    const filter = {};
    if (personId) filter.personId = ObjectID(personId);
    if (teamId) filter.teamId = ObjectID(teamId);
    return filter;
  }
}

export default database => {
  const collection = database.collection(NAME);
  return [
    NAME,
    collection,
    (user, models, options) => new AmendmentModel(NAME, collection, user, models, options),
    {
      indexes: [[{ from: 1 }], [{ teamId: 1 }]],
    },
  ];
};

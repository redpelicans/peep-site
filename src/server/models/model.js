import { ObjectID } from 'mongodb';
import { prop } from 'ramda';
import { ApolloError } from 'apollo-server';
import { isAdmin } from './roles';

export default class Model {
  constructor(name, collection, user, models) {
    this.collection = collection;
    this.name = name;
    this.user = user;
    this.models = models;
  }

  checkAuth() {
    if (!this.user) throw new ApolloError('Unauthorized', 401);
  }

  checkIsAdmin() {
    this.checkAuth();
    if (!isAdmin(this.user)) throw new ApolloError('Unauthorized', 403);
  }

  loadOne(id, params) {
    this.checkAuth();
    return this.collection.findOne({ _id: new ObjectID(id) }, { ...params,  projection: { password: 0 } });
  }

  async count(filter) {
    this.checkAuth();
    return this.collection.find(await this._makeFilter(filter)).count();
  }

  async loadAll({ skip, first, orderBy = 'id_ASC', filter }) {
    this.checkAuth();
    let request = this.collection.find(await this._makeFilter(filter), { projection: { password: 0 } });
    if (first) request = request.skip(skip).limit(first);
    const [_, sortField, sortOrder] = orderBy.match(/(\w+)_(ASC|DESC)$/) || []; // eslint-disable-line
    request.sort({ [sortField === 'id' ? '_id' : sortField]: sortOrder === 'ASC' ? 1 : -1 });
    return request.toArray();
  }

  insertOne(document) {
    this.checkAuth();
    return this.collection.insertOne(document).then(prop('insertedId'));
  }

  insertMany(documents) {
    this.checkAuth();
    return this.collection.insertMany(documents).then(prop('insertedIds'));
  }

  async delete(id) {
    this.checkIsAdmin();
    if (!(await this.loadOne(id))) throw new ApolloError('Unknown document', 404);
    return this.collection.deleteOne({ _id: new ObjectID(id) });
  }
}

const pino = require('pino');
const pinoms = require('pino-multi-stream');
const prettifier = require('pino-pretty');
const stackdriver = require('pino-stackdriver');

let options = {
  name: 'peep',
  level: process.env.NODE_ENV === 'test' ? 'silent' : 'info',
};

const prettyStream = pinoms.prettyStream({
  prettyPrint: {
    colorize: true,
    translateTime: 'SYS:standard',
    ignore: 'hostname,pid',
  },
  prettifier,
});

const streams = [{ stream: prettyStream }];

if (process.env.LOGGING_DRIVER === 'gke') {
  const projectId = process.env.LOGGING_PROJECT_ID;
  const logName = process.env.LOGGING_LOGNAME;
  const stackDriverStream = stackdriver.createWriteStream({
    projectId,
    logName,
    resource: {
      type: 'global',
      labels: {
        app: 'peep',
      },
    },
  });
  streams.push(stackDriverStream);
}

module.exports = pino(options, pinoms.multistream(streams));

import { gql } from 'apollo-server';
import { ApolloError } from 'apollo-server';
import { ObjectID } from 'mongodb';
import { assoc, compose, omit, curry, prop, dissoc } from 'ramda';
import { ObjectIDResolver, DateTimeResolver } from 'graphql-scalars';
import GraphQLJSON, { GraphQLJSONObject } from 'graphql-type-json';
import { GraphQLScalarType } from 'graphql';
// import { Kind } from 'graphql/language';
import { addMinutes, parse, startOfDay, format } from 'date-fns';
import { isPending } from '../models/statuses';

const renameKey = curry((oldKey, newKey, obj) => assoc(newKey, prop(oldKey, obj), dissoc(oldKey, obj)));

export const typeDefs = gql`
  scalar DateTime
  scalar ObjectID
  scalar JSON
  scalar JSONObject
  scalar DMY

  type User {
    id: ID!
    firstname: String
    lastname: String
    fullname: String
    email: String!
    status: UserStatus!
    roles: [UserRole]!
    rights: [UserRight]
    teams: [Team]
    lastLogin: DateTime
  }

  type Team {
    id: ID!
    name: String!
    description: String
    members: [TeamMember]
    country: String!
    type: TeamType
    createdAt: DateTime
    updatedAt: DateTime
    createdBy: ObjectID
    updatedBy: ObjectID
  }

  type SpareDay {
    id: ID!
    from: DateTime!
    to: DateTime
    country: String
    label: String
    team: Team
  }

  type Event {
    id: ID!
    type: EventType!
    from: DateTime!
    to: DateTime!
    team: Team
    person: User!
    description: String
  }

  type Amendment {
    id: ID!
    team: Team!
    person: User!
    from: DateTime!
    to: DateTime!
    days: Int!
    reference: String
    description: String
  }

  type ProjectWorkingDaysStat {
    team: Team!
    from: DateTime!
    to: DateTime!
    worker: User!
    lastAmendment: Amendment
    amendmentWorkingDays: Float
    currentYearWorkingDays: Float
  }

  type CompanyWorkingDaysStat {
    team: Team!
    from: DateTime!
    to: DateTime!
    worker: User!
    workingDaysInPeriod: Float
    vacationDays: Float
    unpaidLeaveDays: Float
    sickLeaveDays: Float
  }


  type PeriodStat {
    from: DateTime!
    to: DateTime!
    teamId: ObjectID
    workingDays: Int!
  }

  type TeamMemberPeriod {
    inceptionDate: DateTime
    expirationDate: DateTime
  }

  type TeamMember {
    person: User
    role: TeamMemberRole
    activityPeriods: [TeamMemberPeriod]
  }

  enum TeamType {
    company
    project
  }

  enum EventType {
    dayOff
    vacation
    unpaidLeave
    sickLeave
    spareDay
    workingDay
  }

  enum TeamMemberRole {
    worker
    headWorker
    observer
    manager
    employee
  }

  enum UserStatus {
    active
    inactive
    pending
  }

  enum UserRole {
    user
    admin
  }

  enum UserRight {
    spareDays
    teamsCreate
  }

  type Mutation {
    createUser(
      firstname: String!
      lastname: String!
      email: String!
      status: UserStatus
      roles: [UserRole]
      rights: [UserRight]
      sendInvitation: Boolean
      password: String
    ): User

    updateUser(
      id: ID!
      firstname: String
      lastname: String
      email: String
      status: UserStatus
      roles: [UserRole]
      rights: [UserRight]
      sendInvitation: Boolean
    ): User

    deleteUser(id: ID!): DeletedDoc

    createTeam(
      name: String!
      description: String
      type: TeamType = project
      country: String = "FR"
      members: [TeamMemberInput]
    ): Team
    updateTeam(
      id: ID!
      name: String!
      country: String
      type: TeamType
      description: String
      members: [TeamMemberInput]
    ): Team
    deleteTeam(id: ID!): DeletedDoc

    createSpareDay(from: DateTime!, to: DateTime, label: String!, country: String, teamId: ObjectID): SpareDay

    createEvent(
      type: EventType!
      from: DateTime!
      to: DateTime!
      personId: ObjectID!
      teamId: ObjectID!
      description: String
    ): Event

    updateSpareDay(id: ID!, from: DateTime, to: DateTime, label: String, country: String): SpareDay

    updateEvent(id: ID!, from: DateTime, to: DateTime, teamId: ObjectID, description: String, type: EventType): Event

    deleteEvent(id: ID!): DeletedDoc
    deleteSpareDay(id: ID!): DeletedDoc

    createAmendment(
      teamId: ObjectID!
      personId: ObjectID!
      from: DateTime!
      to: DateTime!
      days: Int!
      description: String
    ): Amendment

    updateAmendment(
      id: ID!
      teamId: ObjectID
      personId: ObjectID
      from: DateTime
      to: DateTime
      days: Int
      description: String
    ): Amendment

    deleteAmendment(id: ID!): DeletedDoc
  }

  type Query {
    projectWorkingDaysStats(teamId: ObjectID!, from: DateTime!, to: DateTime!): [ProjectWorkingDaysStat]
    companyWorkingDaysStats(teamId: ObjectID!, from: DateTime!, to: DateTime!): [CompanyWorkingDaysStat]
    workingDaysOfPeriod(teamId: ObjectID!, from: DateTime!, to: DateTime!): PeriodStat

    allUsers(first: Int, skip: Int, orderBy: String, filter: UserFilter): [User]!
    searchUsers(match: String): [User]!
    checkEmailUniqueness(email: String!): Boolean
    _allUsersMeta(first: Int, skip: Int, orderBy: String, filter: UserFilter): ListMetadata
    User(id: ID!): User

    allTeams(first: Int, skip: Int, orderBy: String, filter: TeamFilter): [Team]!
    checkTeamUniqueness(name: String!): Boolean
    _allTeamsMeta(first: Int, skip: Int, orderBy: String, filter: TeamFilter): ListMetadata
    Team(id: ID!): Team

    allCalendarSpareDays(teamId: ObjectID!, from: DateTime, to: DateTime): [SpareDay]!
    allCalendarEvents(teamId: ObjectID!, from: DateTime, to: DateTime): [Event]!

    allSpareDays(first: Int, skip: Int, orderBy: String, filter: SpareDayFilter): [SpareDay]!
    _allSpareDaysMeta(filter: SpareDayFilter): ListMetadata
    SpareDay(id: ID!): SpareDay
    checkSpareDayUniqueness(from: DateTime!, country: String, teamId: ObjectID): Boolean!
    allSpareDaysCountries: [String]
    allSpareDaysYears: [String]

    allEvents(first: Int, skip: Int, orderBy: String, filter: EventFilter): [Event]!
    _allEventsMeta(first: Int, skip: Int, orderBy: String, filter: EventFilter): ListMetadata
    Event(id: ID!): Event

    allAmendments(first: Int, skip: Int, orderBy: String, filter: AmendmentFilter): [Amendment]!
    _allAmendmentsMeta(first: Int, skip: Int, orderBy: String, filter: AmendmentFilter): ListMetadata
    Amendment(id: ID!): Amendment
    allAmendmentsTeams: [Team]
    allAmendmentsUsers: [User]
  }

  input TeamMemberPeriodInput {
    inceptionDate: DateTime
    expirationDate: DateTime
  }

  input TeamMemberInput {
    role: TeamMemberRole
    personId: ObjectID
    activityPeriods: [TeamMemberPeriodInput]
  }

  input SpareDayFilter {
    from: DateTime
    to: DateTime
    label: String
    country: String
    teamId: ObjectID
  }

  input EventFilter {
    from: DateTime
    to: DateTime
    teamId: ObjectID
    personId: ObjectID
    types: [EventType!]
  }

  input AmendmentFilter {
    teamId: ObjectID
    personId: ObjectID
  }

  input TeamFilter {
    q: String
    country: String
    userId: ObjectID
  }

  input UserFilter {
    q: String
    status: UserStatus
  }

  type ListMetadata {
    count: Int!
  }

  type DeletedDoc {
    id: ID!
  }
`;

export const resolvers = {
  DateTime: DateTimeResolver,
  ObjectID: ObjectIDResolver,
  JSON: GraphQLJSON,
  JSONObject: GraphQLJSONObject,

  DMY: new GraphQLScalarType({
    name: 'DMY',
    description: 'Date custom scalar type',
    parseValue(dmy) {
      const date = parse(dmy, 'ddMMyy', startOfDay(new Date()));
      return addMinutes(date, date.getTimezoneOffset());
    },
    serialize(date) {
      return format(date, 'ddMMyy');
    },
    parseLiteral() {
      return null;
    },
  }),

  SpareDay: {
    id: event => event._id,
    team: (spareDay, args, { ctx }) => {
      const { dataLoaders } = ctx();
      return dataLoaders.loadTeam.load(ObjectID(spareDay.teamId));
    },
  },

  Event: {
    id: event => event._id,
    person: (event, args, { ctx }) => {
      const { dataLoaders } = ctx();
      return dataLoaders.loadPerson.load(ObjectID(event.personId));
    },
    team: (event, args, { ctx }) => {
      const { dataLoaders } = ctx();
      return dataLoaders.loadTeam.load(ObjectID(event.teamId));
    },
  },

  Amendment: {
    id: amendment => amendment._id,
    person: (amendment, args, { ctx }) => {
      const { dataLoaders } = ctx();
      return dataLoaders.loadPerson.load(ObjectID(amendment.personId));
    },
    team: (amendment, args, { ctx }) => {
      const { dataLoaders } = ctx();
      return dataLoaders.loadTeam.load(ObjectID(amendment.teamId));
    },
  },

  Team: {
    id: team => team._id,
  },

  TeamMember: {
    person: (member, args, { ctx }) => {
      const { dataLoaders } = ctx();
      return dataLoaders.loadPerson.load(ObjectID(member.personId));
    },
  },

  User: {
    id: user => user._id,
    fullname: user => `${user.firstname} ${user.lastname}`,
    teams: (person, args, { user, ctx }) => {
      const { dataLoaders } = ctx();
      return dataLoaders.loadPersonTeams.load({ person, loggedUser: user });
    },
  },

  Mutation: {
    async createUser(_, args, { user: author, ctx, models }) {
      const {
        services: { api },
      } = ctx();
      const user = await models.people.create(omit(['sendInvitation'], args));

      if (isPending(args))
        await api.services.auth.emit('person:invited', {
          output: { id: user._id },
          globals: ctx,
          locals: { user: author },
        });
      return dissoc('password', user);
    },

    async updateUser(_, args, { user: author, ctx, models }) {
      const {
        services: { api },
      } = ctx();
      const user = compose(assoc('_id', args.id), omit(['id', 'sendInvitation']))(args);
      const version = await models.people.updateOne(user);
      if (isPending(args))
        await api.services.auth.emit('person:invited', {
          output: { id: user._id },
          globals: ctx,
          locals: { user: author },
        });
      return dissoc('password', version);
    },

    async deleteUser(_, args, { models }) {
      const amendment = await models.amendments.collection.findOne({ personId: ObjectID(args.id) });
      if (amendment) throw new ApolloError('Cannot delete user with amendment!', 400);
      await Promise.all([
        models.people.delete(args.id),
        models.teams.removeMemberFromAllTeams(args.id),
        models.events.collection.deleteMany({ personId: ObjectID(args.id) }),
      ]);
      return { id: args.id };
    },

    createTeam(_, args, { models }) {
      return models.teams.create(args);
    },

    updateTeam(_, args, { models }) {
      const user = renameKey('id', '_id', args);
      return models.teams.updateOne(user);
    },

    async deleteTeam(_, args, { models }) {
      const day = await models.spareDays.collection.findOne({ teamId: ObjectID(args.id) });
      if (day) throw new ApolloError('Cannot delete a team with spare days!', 400);
      const amendment = await models.amendments.collection.findOne({ teamId: ObjectID(args.id) });
      if (amendment) throw new ApolloError('Cannot delete a team with amendment!', 400);
      const res = await models.teams.delete(args.id).then(() => ({ id: args.id }));

      return res;
    },

    createEvent(_, args, { models }) {
      return models.events.create(args);
    },

    updateEvent(_, args, { models }) {
      const event = renameKey('id', '_id', args);
      return models.events.updateOne(event);
    },

    createSpareDay(_, args, { models }) {
      return models.spareDays.create({ ...args });
    },

    updateSpareDay(_, args, { models }) {
      const event = renameKey('id', '_id', args);
      return models.spareDays.updateOne(event);
    },

    deleteEvent(_, args, { models }) {
      return models.events.delete(args.id).then(() => ({ id: args.id }));
    },

    deleteSpareDay(_, args, { models }) {
      return models.spareDays.delete(args.id).then(() => ({ id: args.id }));
    },

    createAmendment(_, args, { models }) {
      return models.amendments.create(args);
    },
    updateAmendment(_, args, { models }) {
      const amendment = renameKey('id', '_id', args);
      return models.amendments.updateOne(amendment);
    },
    deleteAmendment(_, args, { models }) {
      return models.amendments.delete(args.id).then(() => ({ id: args.id }));
    },
  },

  Query: {
    workingDaysOfPeriod(_, args, { models }) {
      return models.spareDays.workingDaysOfPeriod(args);
    },

    async _allUsersMeta(_, args, { models }) {
      return { count: await models.people.count(args.filter) };
    },

    allUsers(_, args, { models }) {
      return models.people.loadAll(args);
    },

    searchUsers(_, args, { models }) {
      return models.people.search(args);
    },

    checkEmailUniqueness(_, args, { models }) {
      return models.people.checkEmailUniqueness(args.email);
    },

    User(_, args, { models }) {
      return models.people.loadOne(args.id);
    },

    Team(_, args, { models }) {
      return models.teams.loadOne(args.id);
    },

    async _allTeamsMeta(_, args, { models }) {
      return { count: await models.teams.count(args.filter) };
    },

    allTeams(_, args, { models }) {
      return models.teams.loadAll(args);
    },

    checkTeamUniqueness(_, args, { models }) {
      return models.teams.checkTeamUniqueness(args.name);
    },

    Event(_, args, { models }) {
      return models.events.loadOne(args.id);
    },

    async _allEventsMeta(_, args, { models }) {
      return { count: await models.events.count(args.filter) };
    },

    allEvents(_, args, { models }) {
      return models.events.loadAll(args);
    },

    SpareDay(_, args, { models }) {
      return models.spareDays.loadOne(args.id);
    },

    async _allSpareDaysMeta(_, args, { models }) {
      return { count: await models.spareDays.count(args) };
    },

    async allSpareDaysCountries(_, args, { models }) {
      return await models.spareDays.getSpareDaysCountries();
    },
    async allSpareDaysYears(_, args, { models }) {
      return await models.spareDays.getSpareDaysYears();
    },

    allSpareDays(_, args, { models }) {
      return models.spareDays.loadAll(args);
    },

    async allCalendarSpareDays(_, args, { models }) {
      const team = await models.teams.loadOne(args.teamId, { country: 1 });
      if (!team) throw new ApolloError('Cannot get team!', 404);
      const filter = {
        from: args.from,
        to: args.to,
        teamId: args.teamId,
        country: team.country,
      };
      return models.spareDays.loadAll({ filter });
    },

    allCalendarEvents(_, args, { models }) {
      return models.teams.loadAllEvents(args);
    },

    checkSpareDayUniqueness(_, args, { models }) {
      return models.spareDays.checkSpareDayUniqueness(args);
    },

    Amendment(_, args, { models }) {
      return models.amendments.loadOne(args.id);
    },

    async _allAmendmentsMeta(_, args, { models }) {
      return { count: await models.amendments.count(args.filter) };
    },

    async allAmendmentsTeams(_, args, { models }) {
      return await models.amendments.getAmendmentsTeams();
    },

    async allAmendmentsUsers(_, args, { models }) {
      return await models.amendments.getAmendmentsUsers();
    },

    allAmendments(_, args, { models }) {
      return models.amendments.loadAll(args);
    },

    companyWorkingDaysStats(_, args, { models }) {
      return models.teams.getCompanyWorkingDaysStats(args);
    },

    projectWorkingDaysStats(_, args, { models }) {
      return models.teams.getProjectWorkingDaysStats(args);
    },
  },
};
